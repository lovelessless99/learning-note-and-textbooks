# intellij IDEA 建置 Servlet 筆記

>由於 intellij IDEA 實在太好用，但是 Community 版本沒有內建 Java EE 及 Servelt 的自動化建置專案，但是我又不想要付錢專業版的，因此，可以稍微繞一點遠路，就可以在 Community 開發或是練習 Java Servlet 之類的技術。   
>[參考網站 - 在 Intellij IDEA Community Edition 使用 Servlet / JSP ](https://www.shortn0tes.com/2017/01/tutorial-intellij-idea-community.html)

我們這裡會使用到 `Maven` 這個專案管理及自動構建工具，但其實不用知道太多，這裡需要的話以後慢慢學就好，畢竟我們主要目的是快速架設環境給我們練習

首先，我們先建立新的專案，並在右側選擇 `Maven`，勾選上方 `Create from archetype`，選擇有 `Webapp` 的選項，如下。

![NewPRoject](image/NewProject.PNG)

再來，命名 `GroupID` 和 `ArtifactID`，我這裡分別命名成 `org.mycompany.myname` 和 `servletTest`，如下，

![Name](image/Name.PNG)

之後再命名新的專案名稱及路徑，即可開啟新專案。

建好之後，因為 Maven 沒有創建 java 檔案，所以我們要在 專案底下 `src/main/` 創建 java 資料夾，並且設成 `source root` ，是將來`Servlet` 會去的地方，如下

![SourceRoot](image/SourceRoot.png)

設好之後，在剛剛設好的 java 資料夾下，新增一個 package `org.mycompany.myname`，加入一個 java 檔案，貼上以下的 code

```java
package org.mycompany.myname;

import javax.servlet.http.*;
import java.io.IOException;

public class HelloServlet extends HttpServlet {
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws IOException {
        httpServletResponse.getWriter().print("Hello from servlet");
    }
}
```
在這裡，IDE會報錯，因為沒有 javax 的 函式庫
![Error](image/Error.PNG)

所以我們必須要修改 Maven 的 XML 檔案，去叫他自動幫我們載入所需要的相依函式庫，在 `pom.xml` 改成以下的文字

```XML
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>org.mycompany.myname</groupId>
  <artifactId>servletTest</artifactId> <!-- 這裡是專案名稱 -->
  <packaging>war</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>servletTest Maven Webapp</name>
  <url>http://maven.apache.org</url>

    <!-- 這裡是相依套件 JUnit、javax -->
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>javax</groupId>
      <artifactId>javaee-api</artifactId>
      <version>6.0</version>
      <scope>provided</scope>
    </dependency>

  </dependencies>

  <build>
    <finalName>mywebapp</finalName>
    <plugins>
      <plugin>
        <groupId>org.apache.tomcat.maven</groupId>
        <artifactId>tomcat7-maven-plugin</artifactId>
        <version>2.2</version>
        <configuration>
          <port>8888</port>
          <path>/</path>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
    </plugins>
  </build>

</project>

```
改好之後，在 `pom.xml` 按滑鼠右鍵，在 `Maven` 選項選 `reimport`，就可以全部幫你載入，上面的錯誤也都灰飛煙滅啦~~~

再來，我們在 Webapp 資料夾底下創建一個 `index.jsp`，並貼上以下內容

```html
<html>
    <body>
        <h2>
            Hello World!
        </h2>
    </body>
</html>
```

完成設置之後，我們要開始跑了，但到底要怎麼執行呢 ? 首先我們到 `Run/Debug Configurations`，在右上執行按鈕的旁邊點一下

![Debug](image/Debug.PNG)

按 加號 圖案，去新增一個 Maven Configuration，如下。

![addconfigure](image/addconfigure.png)

並且，Working Directory 設為目前專案資料夾，Command line 設為 `tomcat7:run`，名稱叫做 tomcat，並且再如上新增一個新的，Command line 為 `Clean`，名稱叫做 `Clean`，如下

![tomcat](image/tomcat.PNG)

最後，把 `webapp` 資料夾底下的 `WEB-INF/web.xml`，改成這樣
```XML
<!DOCTYPE web-app PUBLIC
        "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
        "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
  <display-name>Archetype Created Web Application</display-name>

  <servlet>
    <servlet-name>HelloServlet</servlet-name>   <!-- 剛剛創建java檔的名稱-->
    <servlet-class>org.mycompany.myname.HelloServlet</servlet-class>
  </servlet>

  <servlet-mapping>
    <servlet-name>HelloServlet</servlet-name>  <!-- 剛剛創建java檔的名稱-->
    <url-pattern>/servlet</url-pattern>
  </servlet-mapping>

</web-app>
```

按 `Shift + F10` 執行，在 Console 點那個網址( `http://localhost:8888/`)，就可以看到以下內容了 ~

![Hello](image/Hello.PNG)

終於可以用了，撒花 (*￣▽￣)/‧☆*"\`'*-.,_,.-*'\`"*-.,_☆

但是有些人嫌，自動化建置後執行還要自己手動按 localhost，式有沒有這麼懶惰，但是這裡提供一些方法可以全部自動化執行，不用再手動按了。

把剛剛 `pom.xml` `<build>`標籤下的 `<plugins>` 改成這樣
```XML
<plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.6.0</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.tomcat.maven</groupId>
        <artifactId>tomcat7-maven-plugin</artifactId>
        <version>2.2</version>
        <executions>
          <execution>
            <id>start-tomcat</id>
            <phase>pre-integration-test</phase>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>

          <execution>
            <id>stop-tomcat</id>
            <phase>post-integration-test</phase>
            <goals>
              <goal>shutdown</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <port>8888</port>
          <path>/</path>
          <fork>true</fork>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <version>1.5.0</version>
        <executions>
          <execution>
            <phase>integration-test</phase>
            <goals>
              <goal>exec</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <executable>test.bat</executable> <!-- 注意這行，等等要創建 test.bat batch 檔案 -->
        </configuration>
      </plugin>
    </plugins>

```

注意 `<executable>test.bat</executable>`，我們在專案跟目錄底下創建 `test.bat` 的 batch 檔案，填入以下內容

```bat
@echo off
start "" http://localhost:8888
pause
```

這個時候按執行會出錯，如下

![autoerror](image/autoError.PNG)

我們要修改 `Run/Debug Configurations`，新增一個 `verify`，如下設定

![verify](image/verify.PNG)

按下執行，就會直接跳出來網頁 ~~，超開心的，終於 !!

最後，你如果想用一個獨立的環境開啟，類似 bracket 那樣另外以匿名模式下開網頁，不影響本地的瀏覽器。請將`test.bat` 改成

```bat
@echo off
start /d "C:\Program Files (x86)\Google\Chrome\Application\" chrome.exe -incognito http://localhost:8888
pause
```

你可以看到這是一個獨立的環境，沒有你之前開的分頁影響。

![incognito](image/incognito.PNG)

