# Maven 筆記

## 一、安裝篇
將Maven下載解壓縮後，加入檔案路徑後，先做測試，再命令列下輸入 `mvn -v`，有反應就是安裝成功囉 ~

再來做個小測試，先在桌面新增資料夾，命令列進入裡面，並輸入

    mvn archetype:generate -DgroupId=com.mkyong.common -DartifactId=RESTfulExample -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false

就會在資料夾底下，創造一個 `RESTfulExample` 專案

`groupId` : 專案所隸屬的組織或公司   

`artifactId` : 專案名稱   

`archetypeArtifactId` : 跟 maven 講說你要用什麼原型(archetype) 來建置專案的結構及初始化，例如你要建立簡單的webapp，後面加`maven-archetype-webapp` 就會幫你創造簡單的web專案。

`interactiveMode` : 看是否要文字介面互動模式，會一個一個問要什麼，有點麻煩

而 Maven 的專案管理設定主要靠 `Pom.xml` 管理，`Pom.xml`是 project 的設定檔。如剛剛專案內的該檔內容(有稍微改一下標籤順序，方便說明)

## 二、Maven的識別管理
Maven的識別管理，分為三層 groupId : artifactId : version，一個組織 (group) 可能存在多個 Project(artifact)，每個Project也可能存在多個版本 (version)， 整個函式庫就以這種檔檔案架構進行處理，當使用的函式不存在時，Maven會到 http://repo1.maven.org/maven2/ 或是 http://repo2.maven.org/maven2/ 進行下載，下載後存到您電腦上的函式庫
``` xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <!--本 Project 識別，也就是剛剛下的命令列參數-->
  <groupId>com.mkyong.common</groupId>
  <artifactId>RESTfulExample</artifactId>
  <version>1.0-SNAPSHOT</version>
  
  <!--表示打包Project的型態,可能為Jar、war、ear或pom，若是使用了android 則為apk-->
  <packaging>war</packaging>

  <!--以下是給工具看的,主要是本Project的資訊-->
  <name>RESTfulExample Maven Webapp</name>
  <url>http://maven.apache.org</url>

  <!--設定引用函式庫-->
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>


  <build>
    <!-- 打包檔的名稱 -->
    <finalName>RESTfulExample</finalName>
  </build>
</project> 
```

Project目錄架構
以建立web程式來說，其目錄架構說明如下：

    Project目錄
    ├ src[程式碼目錄]
    │ │
    │ ├ main[主要目錄]
    │ │ │
    │ │ ├ java[java程式目錄]
    │ │ │ │
    │ │ │ └ [idv.kentyeh.software....程式套件目錄]
    │ │ │
    │ │ ├ resources[資源目錄,會copy到編譯路徑，以web來說，會依目錄層級放到WEB-INF/classes下]
    │ │ │ │
    │ │ │ └ [各種資源(設定)檔...]
    │ │ │
    │ │ └ webapp[web目錄]
    │ │   │
    │ │   └[其它資料…]
    │ │
    │ └ test[測試相關目錄]
    │   │
    │   ├ java[java測試程式目錄]
    │   │ │
    │   │ └ [...]
    │   │
    │   └ resources[測試資源目錄…]
    │
    └ target[各種處理後產生的資料，包含最終生的的打包標的]

但是 ASF (Apache Software Foundation)再厲害，也不可能搜羅所有Library，所以必要的時候，我們必須引用第三方的函式檔案庫， 以下為可能會用到的來源(加入到Pom.xml)：
```xml
<repositories>
    <repository><!--J2ee 最新的函式庫在此-->
        <id>java.net2</id>
        <name>Repository hosting the jee6 artifacts</name>
        <url>http://download.java.net/maven/2</url>
    </repository>
</repositories>
```

接者，命令列進入到該專案資料夾，輸入
    
    mvn clean package javadoc:javadoc

Maven 的指令可以串接，且命令都是由 plugin 所提供，Maven [本身](http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Built-in_Lifecycle_Bindings) 早已知道許許多多的 goal 的定義，當Maven執行特定的 goal時，缺少的 Plugin，Maven 就會自動去 Repository 下載相關的檔案。