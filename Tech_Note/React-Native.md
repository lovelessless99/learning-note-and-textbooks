# React Native 使用筆記

## 一、 watchman 使用方式
1. 開啟 watchman，先開啟虛擬裝置後，在虛擬裝置按 `Ctrl + m`，選取 `Enable Live Reload`
   [參考連結](https://stackoverflow.com/questions/47515695/how-start-watchman-in-react-native-project)

2. 來嘗試一下，先進入到專案目錄資料夾底下的 `/node_modules/react-native/Libraries/NewAppScreen/components`修改 `Header.js`
把 logo opacity 改成 0.9，儲存後會虛擬裝置會立刻改

## 二、使用 React Native 的下載元件
[參考資料](https://www.npmjs.com/package/react-native-sparkbutton)

1. 先去下載該