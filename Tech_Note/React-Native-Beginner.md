# React-Native Install to Run
## 安裝篇
### Android Studio 設定篇
1. 安裝 android studio
2. 在 welcome to Android Studio 的畫面下方有個 Configure，請先點選sdk manager
3. 在 sdk platform 的頁面，先句選下面的 `Show Package Details` 的句選欄，在 Android 9 (Pie) 項目下，請句選以下三點
    *	Android SDK Platform 28
    * Intel x86 Atom_64 System Image 
    *  Google APIs Intel x86 Atom System Image
4. 接下來在點選下一個分頁 SDK Tools 的分頁，也是一樣先句選下面的 `Show Package`，接下來句選下列項目
	*	Android SDK Build-Tools
		*	28.0.3
		*	29.0.0
		*	29.0.1
		*	29.0.2
	*  Android Auto API Simulators
	*  Android Emulator
	*  Android SDK Platform-Tools
	*  Android SDK Tools
5. 完成以上句選後，請按右下 `APPLY` 按鈕會自動安裝方才句選的項目
6. 再來，在 welcome to Android Studio 的畫面下方有個 Configure，請先點選 AVD manager，安裝虛擬手機裝置。這裡就看你要裝啥手機，我是裝 google pixel 3，安裝好後可以先執行看看，**警告: 如果是 unix 系統可能會出現 kvm 權限問題請利用以下指令解決權限問題**

```bash
sudo apt install qemu-kvm
sudo adduser $USER kvm
grep kvm /etc/group
```
如果出現下面的return 表示你成功了

	kvm:x:131:lawrence

### 其他設定篇
1. 安裝 The React Native CLI

 ```bash
 npm install -g react-native-cli
 ```
 
2. 設定 android sdk 環境變數官網是這樣寫
 ```bash
export ANDROID_HOME=$HOME/Android/Sdk   # ***注意這行，請去該目錄確認是否正確***
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
 ```
 
### watchman 安裝篇
watchman 像是 sass 會即時編譯並重新執行的工具，會不斷監聽你個程式碼有沒有變動

[參考文件](https://github.com/MontoyaAndres/react-native-first-app)

執行下列指令安裝
請先安裝下列套件
```bash
sudo apt-get install libtool
sudo apt install m4
sudo apt-get install python-dev python3-dev
```

再安裝 watchman
```bash
git clone https://github.com/facebook/watchman.git
cd watchman
git checkout v4.9.0  # the latest stable release
sudo ./autogen.sh
sudo ./configure
sudo make
sudo make install
```
## 執行篇
首先，啟動 Command Line，先在你要的目錄底下執行
```bash
react-native init AwesomeProject
```
接下來安裝好後進到該目錄底下
```bash
cd AwesomeProject
```
再來，請去打開 android studio configure 的 AVD Manager，先啟動你剛剛安裝的虛擬裝置

**再來，command line 請打開兩個分頁**
第一個分頁
```bash
react-native start
```
第二個分頁
```bash
react-native run-android
```
可以看到你的虛擬機器上面跑出畫面了

## 幹， 終於 = = 🖕🖕🖕🖕🖕🖕🖕🖕 