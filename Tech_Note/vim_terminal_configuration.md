# 把 terminal 和 vim 美化的步驟

## 一、 Gogh
```bash
sudo apt-get install dconf-cli uuid-runtime
```
安裝後輸入
```
bash -c  "$(wget -qO- https://git.io/vQgMr)" 
```
就可以開始選主題了

推薦主題

    1. Toy Chest
    2. Frontend Galaxy
    3. Dracula


## 二、 vim 外觀設定

先安裝 vim
```bash
sudo apt install vim
```

接下來開始設定 vim 的外觀
我們要裝很多東西讓它看起來很潮，潮到出水的那種
讓你身為工程師同時也是 vim 水水

我們的一切都是來自 [Vim-Awesome](https://vimawesome.com) 這個網站



我們預計裝幾個東西

    1. 安裝套件管理工具
       * Vundle

    2. Vim 大整形
       * Vim-colorschemes
       * dracula-theme
       * 其他設定(行號、縮排等等)
    
    3. Vim 不錯的小工具 
       * vim-airline
       * indentline
    
    4. 開發程式專用的套件
       * Tagbar 
       * The Nerd tree
       * youcompleteme
       * Emmet
       * Vim-multiple-cursors
       * python-mode
       * vim-autoformat


哇賽，當水水真的不容易，那我們就開始吧

### (1). Vundle
Vundle 是 Vim 的套件管理工具，有了他可以更方便的自動下載、安裝、更新、管理我們要的套件
[ Vundle 來源 ](https://github.com/VundleVim/Vundle.vim)
[ 參考教學 ](https://blog.gtwang.org/linux/vundle-vim-bundle-plugin-manager/)

首先安裝 `git` `curl`

```
sudo apt-get install git curl
```

接下來安裝 `Vundle`

```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

首先， `.vimrc` 內輸入以下內容

```bash
    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')

    " let Vundle manage Vundle, required
    Plugin 'VundleVim/Vundle.vim'

    " The following are examples of different formats supported.
    " Keep Plugin commands between vundle#begin/end.
    " plugin on GitHub repo
    Plugin 'tpope/vim-fugitive'
    " plugin from http://vim-scripts.org/vim/scripts.html
    " Plugin 'L9'
    " Git plugin not hosted on GitHub
    Plugin 'git://git.wincent.com/command-t.git'
    " git repos on your local machine (i.e. when working on your own plugin)
    " Plugin 'file:///home/gmarik/path/to/plugin'
    " The sparkup vim script is in a subdirectory of this repo called vim.
    " Pass the path to set the runtimepath properly.
    Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
    " Install L9 and avoid a Naming conflict if you've already installed a
    " different version somewhere else.
    " Plugin 'ascenator/L9', {'name': 'newL9'}

    " All of your Plugins must be added before the following line
    call vundle#end()            " required
    filetype plugin indent on    " required
    " To ignore plugin indent changes, instead use:
    "filetype plugin on
    "
    " Brief help
    " :PluginList       - lists configured plugins
    " :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
    " :PluginSearch foo - searches for foo; append `!` to refresh local cache
    " :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
    "
    " see :h vundle for more details or wiki for FAQ
    " Put your non-Plugin stuff after this line
```

接下來，在 `vim` 內輸入 `:PluginInstall`
就會開始安裝了！

接下來就是重頭戲一，我們要用我們 vim 的外觀了喔喔喔，我真的很興奮

----
### (2).  vim-colorschemes
[資源連結] (https://vimawesome.com/plugin/vim-colorschemes-sweeter-than-fiction)

在剛剛的 `call vundle#begin()` 和 `call vundle#end()` 間加入這行：

```bash
Plugin 'flazz/vim-colorschemes'
```
然後，**很重要的步驟**
1. 先存檔
2. 輸入 :source %
3. 輸入 :PluginInstall

如此所需下載套件才會被更新，千萬別忘記

接下來，你可以在 `ls .vim/bundle/vim-colorschemes/colors/` 下發現一堆 `xxx.vim` 檔案，名稱全部都是主題名稱

假設我今天要換 `molokai` 的主題，就在 `.vimrc` 輸入 

```bash
colorscheme molokai
```

嘿，主題就會換了喔～

----
### (3). dracula-theme
[資源連結] (https://vimawesome.com/plugin/dracula-theme-who-speaks)
剛剛有超級多主題給你選，選到你不要不要的，但是如果嫌麻煩的人，接下來提供一個主題 叫做 `dracula` ， 是一個很有名的主題，我個人也是覺的蠻漂亮的，推薦給你


```bash
Plugin 'dracula/dracula-theme'
```
安裝好後在 `.vimrc` 輸入

```bash
colorscheme dracula
```
即可換主題
----
### (4). 其他設定
剛剛所做的只有顏色而已，那行號等其他設定我們真的必須得自己用了
複製貼上下列文字到 `.vimrc` 檔案內

```bash
 " 檔案編碼
 set encoding=utf-8
 set fileencodings=utf-8,cp950
 
 " 編輯喜好設定
 syntax on        " 語法上色顯示
 set nocompatible " VIM 不使用和 VI 相容的模式
 " set ai           " 自動縮排
 set shiftwidth=4 " 設定縮排寬度 = 4
 set tabstop=4    " tab 的字元數
 set softtabstop=4
 set expandtab   " 用 space 代替 tab
  
 set noeb     " 關閉聲音
 set ruler        " 顯示右下角設定值
 set backspace=2  " 在 insert 也可用 backspace
 set ic           " 設定搜尋忽略大小寫
 set ru           " 第幾行第幾個字
 set hlsearch     " 設定高亮度顯示搜尋結果
 set incsearch    " 在關鍵字還沒完全輸入完畢前就顯示結果
 set smartindent  " 設定 smartindent
 set confirm      " 操作過程有衝突時，以明確的文字來詢問
 set history=100  " 保留 100 個使用過的指令
 set cursorline   " 顯示目前的游標位置
 set number
```

**嘿，就可以開始像是 notepad++ 或是 sublime 一樣有語法高亮之類的漂亮的文字編輯器**

----
### (5). Vim-airline 
[資源連結](https://vimawesome.com/plugin/vim-airline)
[Powerline](https://vimawesome.com/plugin/powerline-red)

ㄟㄟㄟ，我也想要像其他人一樣下面有一列東西告訴你那行等詳細資訊
```bash
Plugin 'bling/vim-airline'
```
就有下面那條東西喔

----

### (6). INDENTLINE
[資源連結](https://vimawesome.com/plugin/indentline)
可不可以像一般 IDE 那樣有條直線幫你對齊你的縮排，不然程式碼會萬劫不復的醜到爆炸

```bash
Plugin 'yggdroot/indentline'
```

**打縮排就有4個點(leading space)這裡待補充**

----
### (7).Tagbar
[資源連結](https://vimawesome.com/plugin/tagbar)

這個可以讓你很清楚看到所有類別的架構

```bash
Plugin 'majutsushi/tagbar'
```

之後，安裝 ctag 套件 **(這裡有更多番外篇)**

```bash
sudo apt-get install ctags
```

再來在 `.vimrc` 輸入 

```bash
nmap <F8> :TagbarToggle<CR>
```
之後按 `f8` 可以看到所有檔案類別結構 (`+`, `-` 是展開或收合)

----

### (8). NERD tree
[資源連結](https://vimawesome.com/plugin/nerdtree-red)
可以看檔案樹狀結構很方便的工具

```bash
Plugin 'scrooloose/nerdtree'
```

安裝完後，在 `.vimrc` 內加入

```bash
autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endi
map <C-z> :NERDTreeToggle<CR>f
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
```

之後預設一打開檔案就可以開啟 Nerdtree 了，並且以快捷鍵 `Ctrl + z` 可以開合視窗

----

### (9). VIM-MULTIPLE-CURSORS

[資源連結](https://vimawesome.com/plugin/vim-multiple-cursors)
[Github](https://github.com/terryma/vim-multiple-cursors#faq)

我想要如同 sublime 或是 vscode 一樣可以一次操縱多行，同時修改相同變數等等

```bash
Plugin 'terryma/vim-multiple-cursors'
```

之後，就在 `Ctrl + N` 以及 `Alt + N` 之間做游標數目之切換

不過這裡搞了我好久，記住如果動不了直接按 `Ctrl + C` 破除固有魔咒

Ex1:
改相同名字所有變數
`Ctrl+n` 按到沒有可以匹配的再按 s 一起編輯 

其他之後慢慢研究～

----

### (10). Emmet-Vim
[資源連結](https://vimawesome.com/plugin/emmet-vim)
開發網頁強大的 emmet 指令，事半功倍

```bash
Plugin 'mattn/emmet-vim'
```

Ex1: html:5 輸入後按 `Ctrl + Y + ,` 就跳出來啦，其他跟 emmet 用法一樣

----

### (11). YOUCOMPLETEME 
[資源連結](https://vimawesome.com/plugin/youcompleteme#general-usage)
最後是重頭戲，YOUCOMPLETEME, 擁有一般 IDE 的 智慧提示功能

請先安裝這些套件
```bash
sudo apt install build-essential cmake python3-dev
```
再去安裝 youcompleteme
```bash
Plugin 'valloric/youcompleteme'
```
安裝完之後
```bash
cd ~/.vim/bundle/youcompleteme
python3 install.py --all
```

現在有 C C++ C# Python java Go Rust Javascript 提示

----
# 總結： 最後的 `.vimrc`

```bash
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

"===========我要的套件 ========

Plugin 'flazz/vim-colorschemes'
Plugin 'dracula/dracula-theme'
Plugin 'bling/vim-airline'
Plugin 'yggdroot/indentline'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdtree'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'mattn/emmet-vim'
Plugin 'valloric/YouCompleteMe'
"==============================


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme dracula

" 檔案編碼
set encoding=utf-8
set fileencodings=utf-8,cp950

" 編輯喜好設定
syntax on        " 語法上色顯示
set nocompatible " VIM 不使用和 VI 相容的模式
" set ai           " 自動縮排
set shiftwidth=4 " 設定縮排寬度 = 4
set tabstop=4    " tab 的字元數
set softtabstop=4
set expandtab   " 用 space 代替 tab

set noeb	     " 關閉聲音
set ruler        " 顯示右下角設定值
set backspace=2  " 在 insert 也可用 backspace
set ic           " 設定搜尋忽略大小寫
set ru           " 第幾行第幾個字
set hlsearch     " 設定高亮度顯示搜尋結果
set incsearch    " 在關鍵字還沒完全輸入完畢前就顯示結果
set smartindent  " 設定 smartindent
set confirm      " 操作過程有衝突時，以明確的文字來詢問
set history=100  " 保留 100 個使用過的指令
set cursorline   " 顯示目前的游標位置
set number       " 顯示目前行號 

nmap <F8> :TagbarToggle<CR>

autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endi

map <C-z> :NERDTreeToggle<CR>f
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

set completeopt-=preview
let g:airline_theme = 'dracula'
color dracula
let g:ycm_filetype_whitelist = {
      \ "c":1,
      \ "cpp":1,
      \ "objc":1,
      \ "sh":1,
      \ "zsh":1,
      \ "zimbu":1,
      \ "python":1,
      \ }
```

# 遺珠
1. [VIM-AUTOFORMAT](https://vimawesome.com/plugin/vim-autoformat)
2. [fisa vim config](http://fisadev.github.io/fisa-vim-config/)
3. [將vim打造成source insight](https://ivan7645.github.io/2016/07/12/vim_to_si/)
