# Pytorch 完整安裝教學紀錄
> 有鑑於每次安裝 pytorch 時而成功時而不成功，所以特此來紀錄一下

## 一、 先進入到 Pytorch 的頁面，照著指令下載
[Pytorch](https://pytorch.org/get-started/locally/)

我以下列的需求安裝
* pip
* Linux
* Python3.6
* CUDA 10.0

``` bash
pip3 install https://download.pytorch.org/whl/cu100/torch-1.1.0-cp36-cp36m-linux_x86_64.whl
pip3 install https://download.pytorch.org/whl/cu100/torchvision-0.3.0-cp36-cp36m-linux_x86_64.whl

# Python 3.6
pip3 install https://download.pytorch.org/whl/cu100/torch-1.0.1.post2-cp36-cp36m-linux_x86_64.whl
pip3 install torchvision
```

安裝好後，開始進入下一步

## 二 、安裝 Nvidia 的 driver
[參考網頁](https://www.mvps.net/docs/install-nvidia-drivers-ubuntu-18-04-lts-bionic-beaver-linux/?fbclid=IwAR0LzqQ96wBxt_glMo7tadO8IYJZngsM6J3WmbTpu5rPDHwpYn5Grl3xifI)

1. 首先先下以下指令
```bash
sudo add-apt-repository ppa:graphics-drivers
sudo apt-get update
```

2. 首先先安裝 `ubuntu-drivers-common` 看有啥 driver 可以安裝
``` bash
sudo apt install ubuntu-drivers-common
```

接下來輸入以下指令
``` bash
ubuntu-drivers devices
```
可以看到

    == /sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0 ==
    modalias : pci:v000010DEd00001C06sv00001043sd0000862Cbc03sc00i00
    vendor   : NVIDIA Corporation
    model    : GP106 [GeForce GTX 1060 6GB Rev. 2]
    driver   : nvidia-driver-418 - third-party free
    driver   : nvidia-driver-430 - third-party free recommended
    driver   : nvidia-driver-396 - third-party free
    driver   : nvidia-driver-410 - third-party free
    driver   : nvidia-driver-390 - distro non-free
    driver   : nvidia-driver-415 - third-party free
    driver   : xserver-xorg-video-nouveau - distro free builtin

接下來選有 `recommended` 的那個進行安裝，

```bash
sudo apt-get install nvidia-driver-430
```

## 三、安裝 toolkit

```bash
sudo apt install nvidia-cuda-toolkit
```

## 四、重新開機
```bash
sudo reboot
```

## 五、測試
```Python
import torch
torch.cuda.is_available()
```