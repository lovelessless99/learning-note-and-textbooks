# Jupyter Notebook 遠端連線的教學

>有鑑於深度學習的課需要用到遠端的電腦跑模型，但是我又需要圖形界面才方便我做報告以及顯示實驗結果，所以希望紀錄一下使用筆記以免以後忘記 🌞🌞🌞🌞🌞🌞

### 參考連結
* [5 easy steps to start editing python notebooks over SSH](https://fizzylogic.nl/2017/11/06/edit-jupyter-notebooks-over-ssh/)
* [自己架一個 jupyter remote machine](https://medium.com/@chen.ishi/%E8%87%AA%E5%B7%B1%E6%9E%B6%E4%B8%80%E5%80%8B-jupyter-remote-machine-4de7122ba272)

## 第一步：在本地端連到遠端電腦開啟無瀏覽器模式的 jupyter notebook

先連到遠端電腦 `ssh <username>@<user_address>`
```bash
ssh lawrence@140.113.215.56
```

開啟 jupyter notebook 非瀏覽器模式 (在 port=8080)

```bash
jupyter notebook --no-browser --port=8080
```

![圖片](https://miro.medium.com/max/558/1*oqQbF1oCzcWJ4VvGd7ShlA.png)

現在，遠端電腦已經在 `port=8080` 開啟 jupyter notebook 的服務了

## 第二步：在本地端新開一個終端機以相同 port 再連去遠端
> 自己 port : 對方 port
``` bash 
ssh -N -L 8080:localhost:8080 lawrence@140.113.215.56 
```

這裡要注意一點，當遠端啟動 jupyter notebook 服務時，會顯示兩種網址

      1. http://localhost:8080/?token=f9986a4b318e6363f989054271b6d449a43b8e1845e9f6ac

      2. http://localhost:8080/login?next=%2Ftree%3F

應該要開啟第一個就可以直接進入遠端的目錄，第二個往只會把你導向一個驗證授權的頁面

這一行指令的用途在於「把本地端的 port 8080 與遠端的 port 8080 間行程一個通道(secure tunneling)」，這樣我就可以透過本地端的 port 8080 看到遠端 port 8080 的資料了，需要注意的是本地端的遠端的 port 不一定要一樣，**`-N` 代表這次 ssh 並不會輸入任何指令，這是因為這次 ssh 的目的單純只是建立一個通道以通訊而已**

最後我們就可以透過網頁瀏覽器在本地端的 port 8080 看到遠端 port 8080 的資料了

![圖片](https://miro.medium.com/max/647/1*18QrPxfslaanqGOROQ0kLA.png)

![圖片](https://miro.medium.com/max/650/1*ZgjZYtQeswrjbe_o_UV5PA.png)

現在你的本地端電腦的 jupyter notebook 就可以用遠端的電腦做事，是不是 hen 方便 🥺🥺🥺🥺

----
# Visdom 遠端連線的教學
> Visdom 是 pytorch 的視覺化工具，因為我的模型是要邊訓練邊畫圖，需要耗費較多記憶體資源，我的筆記型電腦每次跑模型同時跑繪圖電腦每次都快 100 度 = = ，所以既然前面 jupyter notebook 是連遠端跑的，所以理應 Visdom 也要在遠端跑，所以跟前面一樣，希望可以用本地端的瀏覽器看遠端的 visdom 界面

### 發現跟剛剛的步驟完全一模模一樣樣

## 第一步：在本地端連到遠端電腦開啟無瀏覽器模式的 jupyter notebook

先連到遠端電腦 `ssh <username>@<user_address>`
```bash
ssh lawrence@140.113.215.56
```

在遠端開啟 Visdom Server (port 預設在 8097)

```bash
python3 -m visdom.server
```

## 第二步：在本地端新開一個終端機以相同 port 再連去遠端

``` bash 
ssh -N -L 8097:localhost:8097 lawrence@140.113.215.56
```

之後在本地端的瀏覽器網址列輸入 `localhost:8097`

嘿你就可以發現可以看到遠端的 8097 頁面 🤩🤩🤩

----
現在你就可以開始使用同時用遠端的 jupyter notebook 和 Visdom 的界面了，可以拯救你的筆電免於變成一顆大火球了幹 🤫🤫🤫🤫