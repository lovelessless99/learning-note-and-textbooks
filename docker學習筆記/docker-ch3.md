# Docker 學習筆記

## Ch3. 常用指令
### 一、列出本地端映像檔

    docker images

### 二、為映像檔添加標籤

    docker tag ubuntu:latest myubuntu:latest
    docker tag ubuntu:latest myubuntu:latesttt
    

你會發現映像檔多了一個標籤

        REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
        myubuntu                   latest              ea4c82dcd15a        2 months ago        85.8MB
        myubuntu                   latesttt            ea4c82dcd15a        2 months ago        85.8MB
        ubuntu                     latest              ea4c82dcd15a        2 months ago        85.8MB

但是他們三個的雜湊碼 `Image ID` 完全一樣，所以實際上是指向同一個映像檔，只是別名不同而已


### 三、搜尋映像檔

搜尋所有`自動建置`且`評價要3星以上`，`含nginx關鍵字的image`

    docker search nginx --automated -s 3

`--automated` : 僅顯示自動建置的映像檔  
`-s 3`、`--stars=3` : 顯示星數3個以上

### 四、刪除映像檔

#### (1) 使用標籤刪除映像檔
當一個映像檔有多個標籤時候，不會影響映像檔

    docker rmi myubuntu:latesttt
    docker rmi myubuntu:latest

但是當只剩一個標籤指向該映像檔的時候，就要注意，如果刪掉就是真的刪掉了😫😫

> ### 陷阱

如果你用

    docker images

只會列出有Tag(有被指到)的映像檔，以我為例，會看到下面的

    REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE

    dockertutorialmaster_web   latest              c781b5755bea        8 days ago          739MB
    postgres                   latest              f9b577fb1ed6        5 weeks ago         311MB
    myhttpd                    latest              55a118e2a010        2 months ago        132MB
    httpd                      latest              55a118e2a010        2 months ago        132MB
    ubuntu                     14.04               f216cfb59484        2 months ago        188MB
    ubuntu                     latest              ea4c82dcd15a        2 months ago        85.8MB
    training/webapp            latest              6fae60ef3446        3 years ago         349MB

但是你後面再加一個 `-a`

    docker images -a

會把所有東西(包含暫存)列出來

    REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE

    dockertutorialmaster_web   latest              c781b5755bea        8 days ago          739MB
    <none>                     <none>              7dfd0a24b61e        8 days ago          690MB
    <none>                     <none>              d603701894ac        8 days ago          690MB
    <none>                     <none>              673405ae1189        8 days ago          690MB
    <none>                     <none>              de5d90d6a8f0        8 days ago          690MB
    <none>                     <none>              792d9c3490df        8 days ago          690MB
    postgres                   latest              f9b577fb1ed6        5 weeks ago         311MB
    httpd                      latest              55a118e2a010        2 months ago        132MB
    myhttpd                    latest              55a118e2a010        2 months ago        132MB
    ubuntu                     14.04               f216cfb59484        2 months ago        188MB
    ubuntu                     latest              ea4c82dcd15a        2 months ago        85.8MB
    <none>                     <none>              26acbad26a2c        15 months ago       690MB
    training/webapp            latest              6fae60ef3446        3 years ago         349MB

你會發現 `rmi` 好像只是刪掉tag而已，有時候會刪不乾淨，怎麼會這樣 ??

答案是，由於 image 被某個 container 引用（拿來執行），如果不將這個引用的 container 銷毀（刪除），那 image 肯定是不能被刪除。

**所以想要刪除執行過的 images 必須首先刪除它的container**

可以用以下的命令看到本機存在的所有容器

     docker ps -a
     
我的電腦看到

    CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS                     PORTS                     NAMES
    
    52a5cde84e51        ubuntu:14.04               "/bin/bash"              8 days ago          Exited (0) 8 days ago                                cranky_cartwright

    07ab0768b63b        dockertutorialmaster_web   "python manage.py ru…"   8 days ago          Up About an hour           0.0.0.0:8000->8000/tcp    dockertutorialmaster_web_1

    a2c7eff56aa5        postgres                   "docker-entrypoint.s…"   8 days ago          Exited (0) 8 days ago                                postgres

    f0ae792b623f        ubuntu                     "/bin/bash"              6 weeks ago         Exited (255) 5 weeks ago                             dbdata

    7010ff56ac55        training/webapp            "python app.py"          6 weeks ago         Exited (255) 5 weeks ago   0.0.0.0:32768->5000/tcp   web

這些都是仍舊存在在本地端的容器，因為他們仍存在，與他們對應的映像檔就不會被刪除 😨

所以我執行以下指令刪除容器(我這裡示範刪除所有容器)

    docker rm $(docker ps -a -q)

這邊要注意的一點是，這個指令**只能**在`powershell` 或是 `Docker QuickStart Terminal`去做執行，如果你在一般的 `Window Command prompt`執行，會得到以下結果

    unknown shorthand flag: 'a' in -a
    See 'docker rm --help'.

這邊搞了我一下，去查資料才知道，`Window Command prompt` 不支持 `inner functions` ，所以要小心一點。
那刪除容器的時候，如果你得到下面的資訊，表示該容器正在背景運作，不能刪掉

    Error response from daemon: You cannot remove a running container 07ab0768b63bf22c1bd1a3ac835a253cd4dcac607901661648230c22957f3dec. Stop the container before attempting removal or force remove

你這個時候應該要暫停容器執行，再進行刪除容器的動作

    docker stop 07ab07
    docker rm 07ab07

之後你就可以看到容器被刪除囉 ~

最後，如果還有出現有 None 的 images，
根本的原因就是，還有其他沒有刪除的image相依著他們。
以我為例，原本嘗試過刪掉container，再照stackoverflow 人說怎樣可以一次全部刪掉 None 的那些images，發現怎麼試都行不通，我想說再刪刪看其他的images，結果我一移除，他們也就跟著移除。

>#### 小總結
> 遇到 None的，可能有兩種原因  
> 1. 其他 images相依這些 None 的 images
> 2. 該 images 的容器仍然存在，造成無法移除

## Ch4. 操作 Docker 容器

> container 是 images 的一個執行實例(instance)，image是一個靜態的唯讀(Read-only)檔案，而容器帶有執行時需要的可寫入層。  
>
> 如果將 Virtual Machine 想成模擬執行的一整套作業系統(核心、執行應用系統必要環境和其他系統環境)和跑在上面的應用，那麼 Docker Container 就是獨立執行一個(或是一組)應用系統，以及他們的必要環境


一、新建容器

    docker create -it ubuntu:latest

你會看到Console會回覆

    a93150d3d4fe142c7ad3196ceb14554ed3d0850687a0acf6eef47f3c954264f5

就是這個新建容器的 `Container ID`，你再輸入 `docker ps -a`看本機上存在的所有容器。

    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    a93150d3d4fe        ubuntu:latest       "/bin/bash"         13 seconds ago      Created                                 zealous_noyce

而因為我們 create 的 container 是處於停止狀態，我們要啟動它。

    docker start a93150

再用 `docker ps` 看正在執行中的容器，就可以看到我們剛剛啟動的容器了

如果你覺得要 Create 又要 start 很麻煩，可以使用一下命令

    docker run ubuntu:latest /bin/echo 'Hello world!'

如果你要停止容器

    docker stop a93150

刪除容器

    docker rm a93150

注意一點，docker rm 只能刪除停止或是退出狀態的容器，如果你要刪除正在執行中的容器，需在後面加個 `-f`
，Docker 會發送 `SIGKILL` 訊號給容器，終止其中的應用程式，再強行刪除。

## Ch6. Docker 資料管理
容器中管理資料的方式有兩種

* `資料卷 (Data Volumn)` : 容器內資料直接對應到本地主機環境
*  `資料卷容器 (Data Volumn Container)` : 使用特定容器維護資料卷

### 關於資料卷

    1. 類似Linux 中的 mount 操作

    2. 資料卷可以再容器之間共用或是重用，容器之間傳遞資料會變得很方便

    3. 資料卷修改資料會立即生效，不管在容器內或是本地端修改皆是

    4. 資料卷更新不會影響映像檔，將資料和應用程式去耦合

    5. 資料卷會一直存在，直到沒有容器使用，便可以安全的移除它


### 一、在容器內建立一個資料卷
在 `docker create` 或 `docker run`，使用 `-v` 參數會在主機端建立一個目錄並掛載到容器內。例如我這裡新建一個 web 容器，並且新增一個資料卷掛載到容器的 /webapp 目錄

    docker run -d -P --name web -v /webapp training/webapp python app.py

`-d` : 是否在後台背景執行容器  

`-P` : 容器服務的連接 port 對外開放，自動對應到本機的 port  

`--name` : 指定容器的別名

`-v` : 掛載目錄到容器內


### 二、掛載本機目錄作為資料卷

我這裡使用windows的範例，和上面只差一點點

    docker run -d -P --name web_localVolumn -v /C/Users/ASUS/Desktop/webapp:/opt/webapp training/webapp python app.py

其中`/C/Users/ASUS/Desktop/webapp`是對應到`windows`的 `C:/Users/ASUS/Desktop/webapp`目錄，如果本機要掛載的目錄`/opt/webapp`不存在，docker 會幫你創建一個

### 三、資料卷容器
如果你有一些持續更新的資料需要在容器之間共享，最好建立資料卷容器。
資料卷容器，其實就是一個正常的容器，專門用來提供資料卷供其它容器掛載的。

首先，建立一個命名的資料卷容器 dbdata，並新增一個資料卷掛載到 /dbdata：

    docker run -it -v /dbdata --name dbdata ubuntu

其中  
`-i, --interactive=true` : 開啟標準輸入接受使用者輸入命令

`-t, --tty=true` : 分配虛擬終端

兩個參數合併在一起，變成  

`-it` : 維持標準輸入開啟，並且分配一個虛擬終端  

因此我們就進入容器了，並且可以跟容器進行互動

你可以看到輸完上面的指令後，出現了互動模式

    root@aefb64fbcf83:/#

`aefb64fbcf83` 則是剛剛輸入完指令後，創造出來的container ID

**這裡先說明，如果想要出去互動模式，輸入 `exit` 即可**

    root@aefb64fbcf83:/# exit

接著，你輸入 `ls`查看目錄

    root@aefb64fbcf83:/# ls

    bin  boot  dbdata  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var

可以看到 dbdata 的目錄被創建出來了(一開始沒有)

再來，我們新增容器 `db1` 去掛載這個dbdata容器中的資料卷

    docker run -it --volumes-from dbdata --name db1 ubuntu

    root@8877507682a4:/#
這兩個容器都掛載到相同 /dbdata 目錄，三個容器在該目錄下的改動或是寫入其他容器皆可以看到

在 dbdata Container 在 /dbdata 新增一個 test 檔案

    root@aefb64fbcf83:/# cd /dbdata/
    root@aefb64fbcf83:/dbdata# touch test
    root@aefb64fbcf83:/dbdata# ls
    test

我們在db1容器內查看它:

    root@8877507682a4:/# ls /dbdata
    test

我們可以使用 `--volumes-from` 參數從很多個容器掛載很多個資料卷，並且用這個參數所掛載的容器不需要在執行狀態

我們也可以用已經掛載資料卷的容器掛載資料卷
例如剛剛`db1` 已經從 `dbdata` 容器掛載資料卷了

我們可以利用 `volumns-from db1` 來掛載 `dbdata` 容器的 `/dbdata`目錄

    docker run -it --volumes-from db1 --name db3 ubuntu

    root@a06f7e30918a:/# ls /dbdata
    test

### 四、利用資料卷容器搬移資料

(1) 利用下面的命令來備份 dbdata 資料卷容器內的資料卷

     docker run --volumes-from dbdata -v $(pwd):/backup --name worker ubuntu tar cvf /backup/backup.tar /dbdata

我將這段指令碼分成幾段陳述

`--name worker ubuntu` : 利用 `ubuntu` image 建立一個容器 `worker`

`--volumes-from dbdata` : 讓 `worker` 容器掛載 `dbdata` 容器的資料卷 ( `dbdata` 容器內的 `/dbdata`目錄)

`-v $(pwd):/backup` : 將本機目前的執行目錄掛載到 `worker` 容器的 `/backup` 目錄

`docker run` : 啟動 `worker` 容器

`tar cvf /backup/backup.tar /dbdata` : 容器執行後，執行此命令，將 `/dbdata` 底下的內容備份到 `worker` 容器內的 `/backup/backup.tar`，即 `Host` 目前執行目錄下的 `backup.tar`

基本上都是 Linux 的指令，所以 Linux 常用指令很熟的話用這個應該會蠻容易理解的。

另外，當我打這行指令，會出現以下的錯誤
    
    ASUS@DESKTOP-7LJTFQ8 MINGW64 /c/Program Files/Docker Toolbox$ docker run --volumes-from dbdata -v $(pwd):/backup --name worker ubuntu tar cvf /backup/backup.tar /dbdata

    C:\Program Files\Docker Toolbox\docker.exe: invalid reference format: repository name must be lowercase.
    
    See 'C:\Program Files\Docker Toolbox\docker.exe run --help'.

原因是因為當前目錄都要小寫， 我的當前目錄是` C:\Program Files\Docker Toolbox\` ，你想查看你的當前目錄，這個 docker termininal 相當於 Linux 環境，你使用

    pwd 

可以看到當前目錄，`pwd` 是 `Print Working Directory` 的縮寫，也就是顯示目前所在目錄的指令

    /c/Program Files/Docker Toolbox

**Docker Toolbox 不是小寫**

所以我就把當前目錄移到桌面，新增一個 `kkk`資料夾測試

注意不可以用 `cd C:\Users\ASUS\Desktop\kkk` 這是在 `Windows Prompt Command` 的用法，我現在在 Docker 終端機，所以要用 Linux 指令

    cd /C/Users/ASUS/Desktop/kkk

把當前目錄移到 `kkk(名字都小寫)`，執行命令就成功囉

當你進入容器(進入容器)

    ASUS@DESKTOP-7LJTFQ8 MINGW64 /C/Users/ASUS/Desktop/kkk (master)$ docker exec -it 647 bash

    root@647ace801dd5:/# ls
    backup  bin  boot  dbdata  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var

    root@647ace801dd5:/# ls /backup/
    backup.tar

可以看到，`/backup/` 下有 `backup.tar`

----
> ### 後來發現這樣打有用，不然都沒辦法啟動容器
    docker run -t -d --volumes-from dbdata -v $(pwd):/backup --name worker ubuntu
>關於啟動容器，進入容器的方法我還沒用的很熟
----

## 五、恢復資料
如果要恢復資料到一個容器，首先建立一個帶有資料卷的容器 `dbdata2`

    docker run -v /dbdata --name dbdata2 ubuntu /bin/bash

然後建立另一個容器，掛載 `dbdata2` 的容器，並使用 `untar` 解壓備份檔案到掛載的容器卷中

    docker run --volumes-from dbdata2 -v $(pwd):/backup busybox tar xvf/backup/backup.tar

## Ch7. 連接 port 對應與容器相連
### 一、從外部存取容器應用程式

啟動容器的時候，如果不指定 `port` 對應參數，容器外是無法透過網路來存取容器內的各項網路服務和應用

當容器值星一些網路應用程式時，要讓外部存取這些服務，可以透過 `-P` `-p` 參數來指定 `port` 對應， 如果只用 `-P` 則 Docker 會隨機對應容器內部有開放的 `port`

    docker run -d -P training/webapp python app.py
    docker ps -l

`-l` 、 `--latest` : 顯示最後創造的容器  (如果不知道參數何意 docker ps --help)

你可以看到
    
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                     NAMES
    d01e69fd0038        training/webapp     "python app.py"     15 seconds ago      Up 13 seconds       0.0.0.0:32768->5000/tcp   wizardly_wiles

本機的 `TCP:32768 port` 對應到容器的 `TCP:5000 port`
所以只要存取 Host 主機 32768 連接 port，就可以開啟容器內 web 應用系統提供的畫面

### 好，問題來了，要怎麼連到那邊的 Port ??

>## 這裡書上都沒提到，這裡是我自己的解釋

首先，先輸入
    
    docker-machine ls 

我的主機得到

    NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
    default   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.09.0

我們的 docker 是執行在 virtualbox 上，因為我們是用 windows，但是我的電腦不支援 Hyper-v 虛擬化技術，所以要用虛擬機驅動程式去支援本機啟動虛擬機器linux的環境給 docker 執行，為 docker 主機

因此，我們之前使用 docker 會用 `docker quickstart terminal ` ，去利用 virtual box 啟動一個 docker machine 給我們使用。因此我們剛剛對接的 port 其實是 docker container 和 我們的 docker machine 之間的 port 去做對接。 
因此，我們用以上指令可以看到我的 docker machine 主機 ip 是
`192.168.99.100`

或是用我們以下的指令看我們現在使用的 docker-machine ip

    docker-machine ip

可以看到輸出

    192.168.99.100

因此，得到我們和容器對接的ip位址後，用你的瀏覽器，輸入
`ip:port` 

    192.168.99.100:32768

可以看到有 Hello world! 的網頁出現喔，終於😭😭😭

可能是我目前的對於docker 還是初學者，所以覺得荊棘有點多。

再來，**我們可以自己指定 port** ，利用

    docker run -d -p 5000:5000 training/webapp python app.py
把我們本地端(使用的docker machine) port 5000 去對接容器的port 5000

我在瀏覽器，輸入(以我自己 ip 為例子)

    192.168.99.100:5000

一樣得到 Hello world! 的網頁

也可以連續使用 `-p` 綁定多個 port

     docker run -d -p 3200:3200 -p 3000:80 training/webapp python app.py

這時你用 `docker ps -l` 看目前執行的容器

    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                                    NAMES
    d178dcd74e61        training/webapp     "python app.py"     5 seconds ago       Up 3 seconds        0.0.0.0:3200->3200/tcp, 5000/tcp, 0.0.0.0:3000->80/tcp   vigilant_archimedes

可以看到這個容器使用兩個 port 

**你也可以連 ip 也自己指定**，例如:

    docker run -d -p 127.1.0.3:1200:1200 training/webapp python app.py

#### 但是要怎麼看 Hello world !我還沒想出方法，這裡做個記號

### 二、互連機制實現快速存取

容器間的互聯機制是一種讓多個容器內應用程式進行快速溝通的方式。他會在來源與接收容器之間建立連接關係，接收容器可以透過容器名稱快速存取到來源容器，而且不用指定具體的IP

(1). 自定容器名稱  

優點如下
    
1. 容器一看名字就知道是做什麼用  
2. 需要連接到其他容器時，需要重新啟動時，也可以使用容器名稱而無須改變，例如 web 容器 要連接到 db 容器

使用 `--name` 參數就可以幫容器命名

    docker run -d -P --name web training/webapp python app.py

(2). 容器間互連

使用 `--link` 可以讓容器之間安全的相互連通

    docker run -d --name db training/postgres
    docker run -d -P --name web --link db:db training/webapp python app.py

`--link name:alias` : name 是要連接的容器名稱，而 alias是要讓連接的容器識別用的別名

因此，可以讓 web 和 db　建立起連接關系

再來，你可以使用 `env` 指令查看 web 容器的環境變數
這裡我新創一個 web2 容器，但因為加上 `--rm`，表示執行完後離開 (因為沒有加上 `-d` )會自動刪除容器

    docker run --rm --name web2 --link db:db training/webapp env

輸出

    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    HOSTNAME=d0d817a2d8c4
    DB_PORT=tcp://172.17.0.2:5432
    DB_PORT_5432_TCP=tcp://172.17.0.2:5432
    DB_PORT_5432_TCP_ADDR=172.17.0.2
    DB_PORT_5432_TCP_PORT=5432
    DB_PORT_5432_TCP_PROTO=tcp
    DB_NAME=/web2/db
    DB_ENV_PG_VERSION=9.3
    HOME=/root

`DB_` 開頭是提供 web 容器連接 db 容器使用的，這個來自我們連接的 db 容器的 alias (剛剛的 `--link name:alias`)

除了環境變數外，docker 還將 host 資訊加到連接容器的 /etc/host

    docker run -i -t --rm --name web2 --link db:db training/webapp /bin/bash

可以看到

    root@ca8e836b7129:/opt/webapp# cat /etc/hosts
    127.0.0.1       localhost
    ::1     localhost ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    172.17.0.2      db eabdeb46e6b3
    172.17.0.4      ca8e836b7129

`172.17.0.2` 是 db 容器的 IP，後面的是主機 ID  
`172.17.0.4` web 容器用自己的容器 ID 作為預設的主機名稱

我們可用 ping 指令確定和 db 容器網路是否暢通

    root@ca8e836b7129:/opt/webapp# ping db

它會自動將 db 解析成 `172.17.0.2`

    64 bytes from db (172.17.0.2): icmp_seq=1 ttl=64 time=0.083 ms
    64 bytes from db (172.17.0.2): icmp_seq=2 ttl=64 time=0.088 ms
    64 bytes from db (172.17.0.2): icmp_seq=3 ttl=64 time=0.055 ms
    64 bytes from db (172.17.0.2): icmp_seq=4 ttl=64 time=0.056 ms
    64 bytes from db (172.17.0.2): icmp_seq=5 ttl=64 time=0.058 ms
    64 bytes from db (172.17.0.2): icmp_seq=6 ttl=64 time=0.067 ms
    64 bytes from db (172.17.0.2): icmp_seq=7 ttl=64 time=0.059 ms
    64 bytes from db (172.17.0.2): icmp_seq=8 ttl=64 time=0.095 ms
    64 bytes from db (172.17.0.2): icmp_seq=9 ttl=64 time=0.061 ms
    64 bytes from db (172.17.0.2): icmp_seq=10 ttl=64 time=0.093 ms
    64 bytes from db (172.17.0.2): icmp_seq=11 ttl=64 time=0.068 ms

    ...

    64 bytes from db (172.17.0.2): icmp_seq=48 ttl=64 time=0.062 ms

    --- db ping statistics ---
    48 packets transmitted, 48 received, 0% packet loss, time 48159ms
    rtt min/avg/max/mdev = 0.039/0.071/0.127/0.024 ms

請自己按 `Ctrl+C` 中止，