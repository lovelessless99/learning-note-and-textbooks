# CPE 常用技巧 - 字串篇

## 一、字元處理

1. 常用字元 ASCII :

char     | Value    |
---------|----------|
 space   | 32       |
 0 - 9   | 48 - 57  |
 A - Z   | 65 - 90  |
 a - Z   | 97 - 122 |

2. 大小寫轉換

    (1) 大寫  

    1. 加 `'a' - 'A'`
    ``` C++
    char s = 'F';
    s = s + ('a' - 'A') ; // f
    ```

    2. 加 32 ( 'a' - 'A' = 97 - 65 = 32)
    ``` C++
    s = s + 32 ; // f
    ```

    3. 引入 tolower()、引入 cctype
     ```C++
        #include <iostream>
        #include <cctype>
        using namespace std;
        int main()
        {
            char t = tolower('F');
            cout << t ;	
        } 
    ```

    (2) 小寫  

    1. 減 `'a' - 'A'`
    ``` C++
    char s = 'f';
    s = s - ('a' - 'A') ; // F
    ```

    2. 減 32 ( 'a' - 'A' = 97 - 65 = 32)
    ``` C++
    s = s - 32 ; // F
    ```

    3. 引入 toupper()、引入 cctype
     ```C++
        #include <iostream>
        #include <cctype>
        using namespace std;
        int main()
        {
            char t = toupper('f');
            cout << t ;	
        } 
    ```

    判斷字元大小寫

    ``` C++
    isalnum(c) : 是否是字母或是數字
    isalpha(c) : 是否是字母
    isdigit(c) : 是否是數字
    islower(c) : 是否小寫
    isupper(c) : 是否大寫
    ispunct(c) : 是否標點符號
    isspace(c) : 是否是空格
    isxdigit(c): 是否 16 進位 
    ```

## 二、字串操作

(1) 初始化

``` C++
    string s1; // 空字串
    string s2(5, 'a'); //aaaaa
    string s3(s2); // s2的拷貝
    string s4(s2.begin(), s2.begin() + s2.size()/2 );
    string s5(s2, 2); // aaa
    string s6(s2, 0, 2); //aa
    string s7(s2, 0, 100000); //aaaaa
```

(2) 特有操作
``` C++
    string s1 = "abcdefg";
    string s2 = "zzzzzzz";

    s1.insert(1, 5, 'y'); // ayyyyybcdefg

    s1.insert(1, s2); // azzzzzzzbcdefg

    s1.insert(1, s2, 3, 5); //azzzzbcdefg

    s1.assign(s2); // zzzzzzz

    s1.erase(2, 3); // abfg

    s1.substr(2, 4); // cdef

    s1.substr(2); // cdefg

    s1.substr()  //abcdefg s1的 copy 

    s1.append("_piggy"); // abcdefg_piggy

    s1.replace(2, 5, "fuxk"); //abfuxk = erase(2, 5) + insert(5, "fuxk")
```

(3) 字串的搜尋

相關的函式會返回一個 `string::size_type`，是一個索引(index)，但若是沒有匹配，會返回一個特殊值`string::npos`，string 定義 npos 為絕對比任何有效索引還大的值。

``` C++

    s.find(args)  // 第一次出現 args 的地方

    s.find(args, pos) // 從 s pos 位置開始找字元

    s.rfind(args) // 最後一次出現 args 的地方

    s.find_first_of(args) // 第一次出現 args內任何字元 的地方

    s.find_last_of(args) // 最後一次出現 args內任何字元 的地方

    s.find_first_not_of(args) // 第一次出現 不屬於 args 內任何字元 的地方

    s.find_last_not_of(args) // 最後一次出現 不屬於 args 內任何字元 的地方

```

以上皆可以給搜尋起始位置的參數

(4) 字串的比較

```C++

    s1.compare(s2) // > 返回 1 , < 返回 -1, == 返回 0
    s1.compare(0, 4, s2); //s1 0 ~ 0+4 位置比較
    s1.compare(0, 4 ,s2, 1, 2) //s1 0 ~ 0+4 位置 和 s2 1 ~ 1+2 位置比較

```

## 三、字串的進階操作

(1) 字串分割 (split by delimiter)

``` C++
#include <iostream>
#include <iterator>
#include <vector>
#include <string> 
using namespace std;
int main()
{ 	
	string s1 = "a b c d e f g h i j k";
	string delimiter = " ";
	vector<string> split_array; 
	size_t pos = 0;
	while( (pos = s1.find(delimiter)) != string::npos ){
		string token = s1.substr(0, pos);
		split_array.push_back(token);
		s1.erase(0, pos + delimiter.length());
	}

	copy(split_array.begin(), split_array.end(), ostream_iterator<string>(cout, "\n"));
}
```
輸出

    a
    b
    c
    d
    ...
(2) 字元轉字串

```C++
    string s1(1,'a'); // a 是字元

    string s1("a"); // a 是字元 array
```

(3) 字元整數轉整數數字

因為字元 0 值為 48，所以 -48 值就是0了

```C++
#include <iostream>
#include <iterator>
#include <vector>
#include <string> 
using namespace std;
int main()
{ 
    string s1 = "0123456789";

    vector<int> array; 

    for(size_t pos = 0; pos < s1.size(); pos++)
        array.push_back(s1[pos] - 48); // or array.push_back(s1[pos]-'0');

    copy(array.begin(), array.end(), ostream_iterator<int>(cout, " "));
}
```

輸出

    0 1 2 3 4 5 6 7 8 9

p.s C++ 11 提供的 `stoi` 系列也很方便，還可以轉任一基底(
[更多stoi看這](http://www.cplusplus.com/reference/string/stoi/) )
所以考試時可先用此測測看是否為 C++ 11

(4) 字串轉整數
上一個教的是一個一個轉整數，這邊教的是要將完整的數字字串轉換，有 atoi 可用，但是是 C++ 11才提供，且下面的方法更快

``` C++
#include <iostream>
#include <vector>
#include <string> 
using namespace std;
int main()
{ 	
    string s = "12345678";
    int sum = 0;

    for(string::size_type i = 0; i < s.length(); i++){
        sum = sum*10 + s[i] - 48;
    }
    cout << sum;
} 
```

(5) 整數轉字串

這裡提供很方便的函式庫 `<sstream>`，為字串流。繼承自 iostream 的操作，也接收單一 string 的建構式，使用如下

```C++
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string> 
using namespace std;
int main()
{ 
    int a ;
    cout << "輸入 : " ;  cin >> a;
    stringstream  stream; stream << a;
    cout << "輸出 : " << stream.str();
}
```

輸出

    輸入 : 2321
    輸出 : 2321

若想要清空 stringstream buffer，利用

    stream.str(std::string());

    或

    stream.str("");

即可清除，因為stringstream 也接收單一 string 的建構式，所以上面利用空字串重新建構新的 stream
