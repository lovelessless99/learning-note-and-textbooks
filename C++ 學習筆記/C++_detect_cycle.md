# 在一個 graph 中偵測有沒有迴圈的技巧

## Detect Cycle (偵測圖中是否有 Cycle )
1. DFS / BFS  `O(V+E)`
2. Union-find `O(ELogV)`
3. 著色法

[兩者效率之比較](https://stackoverflow.com/questions/45272145/finding-cycles-dfs-versus-union-find)

### 1. 深度優先搜尋找Cycle
[參考資料](https://www.geeksforgeeks.org/detect-cycle-undirected-graph/)
```C++
// A C++ Program to detect cycle in an undirected graph 
#include<iostream> 
#include <list> 
#include <limits.h> 
using namespace std; 
  
// Class for an undirected graph 
class Graph 
{ 
    int V;    // No. of vertices 
    list<int> *adj;    // Pointer to an array containing adjacency lists 
    bool isCyclicUtil(int v, bool visited[], int parent); 
public: 
    Graph(int V);   // Constructor 
    void addEdge(int v, int w);   // to add an edge to graph 
    bool isCyclic();   // returns true if there is a cycle 
}; 
  
Graph::Graph(int V) 
{ 
    this->V = V; 
    adj = new list<int>[V]; 
} 
  
void Graph::addEdge(int v, int w) 
{ 
    adj[v].push_back(w); // Add w to v’s list. 
    adj[w].push_back(v); // Add v to w’s list. 
} 
  
// A recursive function that uses visited[] and parent to detect 
// cycle in subgraph reachable from vertex v. 
bool Graph::isCyclicUtil(int v, bool visited[], int parent) 
{ 
    // Mark the current node as visited 
    visited[v] = true; 
  
    // Recur for all the vertices adjacent to this vertex 
    list<int>::iterator i; 
    for (i = adj[v].begin(); i != adj[v].end(); ++i) 
    { 
        // If an adjacent is not visited, then recur for that adjacent
        // 注意父節點替換
        if (!visited[*i]) 
        { 
           if (isCyclicUtil(*i, visited, v)) 
              return true; 
        } 
  
        // If an adjacent is visited and not parent of current vertex, 
        // then there is a cycle. 
        //(如果下一個節點背拜訪過而且不是父節點)
        else if (*i != parent) 
           return true; 
    } 
    return false; 
} 
  
// Returns true if the graph contains a cycle, else false. 
bool Graph::isCyclic() 
{ 
    // Mark all the vertices as not visited and not part of recursion 
    // stack 
    bool *visited = new bool[V]; 
    for (int i = 0; i < V; i++) 
        visited[i] = false; 
  
    // Call the recursive helper function to detect cycle in different 
    // DFS trees 
    for (int u = 0; u < V; u++) 
        if (!visited[u]) // Don't recur for u if it is already visited 
          if (isCyclicUtil(u, visited, -1)) 
             return true; 
  
    return false; 
} 
  
// Driver program to test above functions 
int main() 
{ 
    Graph g1(5); 
    g1.addEdge(1, 0); 
    g1.addEdge(0, 2); 
    g1.addEdge(2, 0); 
    g1.addEdge(0, 3); 
    g1.addEdge(3, 4); 
    g1.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    Graph g2(3); 
    g2.addEdge(0, 1); 
    g2.addEdge(1, 2); 
    g2.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    return 0; 
} 
```
注意，有 else if 的時候 if 如果是巢狀 if，最好要大括弧。因為會默認上一個 if ，很難 debug ~

```C++
if (!visited[*i]) 
{ 
    if (isCyclicUtil(*i, visited, v)) 
        return true; 
} 

else if (*i != parent) 
    return true;
```
還有，傳遞 visited 陣列，要用 call by reference 傳遞本體。

### 2. 廣度優先搜尋找Cycle
[參考資料](https://www.geeksforgeeks.org/detect-cycle-in-an-undirected-graph-using-bfs/)
```C++
#include <bits/stdc++.h> 
using namespace std; 
  
void addEdge(vector<int> adj[], int u, int v) 
{ 
    adj[u].push_back(v); 
    adj[v].push_back(u); 
} 
  
bool isCyclicConntected(vector<int> adj[], int s, 
                        int V, vector<bool>& visited) 
{ 
    // Set parent vertex for every vertex as -1. 
    vector<int> parent(V, -1); 
  
    // Create a queue for BFS 
    queue<int> q; 
  
    // Mark the current node as visited and enqueue it 
    visited[s] = true; 
    q.push(s); 
  
    while (!q.empty()) { 
  
        // Dequeue a vertex from queue and print it 
        int u = q.front(); 
        q.pop(); 
  
        // Get all adjacent vertices of the dequeued 
        // vertex s. If a adjacent has not been visited, 
        // then mark it visited and enqueue it. We also 
        // mark parent so that parent is not considered 
        // for cycle. 
        for (auto v : adj[u]) { 
            if (!visited[v]) { 
                visited[v] = true; 
                q.push(v); 
                parent[v] = u; 
            } 
            else if (parent[u] != v) 
                return true; 
        } 
    } 
    return false; 
} 
  
bool isCyclicDisconntected(vector<int> adj[], int V) 
{ 
    // Mark all the vertices as not visited 
    vector<bool> visited(V, false); 
  
    for (int i = 0; i < V; i++) 
        if (!visited[i] && isCyclicConntected(adj, i, 
                                         V, visited)) 
            return true; 
    return false; 
} 
  
// Driver program to test methods of graph class 
int main() 
{ 
    int V = 4; 
    vector<int> adj[V]; 
    addEdge(adj, 0, 1); 
    addEdge(adj, 1, 2); 
    addEdge(adj, 2, 0); 
    addEdge(adj, 2, 3); 
  
    if (isCyclicDisconntected(adj, V)) 
        cout << "Yes"; 
    else
        cout << "No"; 
  
    return 0; 
} 
```

也可以拿之前的深度優先搜尋來改

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
class Graph{
    private:
        int V;
        list<int> *adj;
        bool isCyclicUtil(int v, vector<bool> &visited); 
    public:
        Graph(int V):V(V), adj(new list<int>[V]){}
        void addEdge(int v, int w);
        bool isCyclic();
};

void Graph::addEdge(int v, int w){
	adj[v].push_back(w);
	adj[w].push_back(v);
}

bool Graph::isCyclicUtil(int v, vector<bool>& visited){
    vector<int> parent(V , -1);
    list<int>::iterator iter;
    queue<int> q;

    visited[v] = true; 
    q.push(v);

    while(!q.empty()){
        int u = q.front(); 
        q.pop();
        
        for(iter = adj[u].begin(); iter!=adj[u].end(); iter++){
            int current = *iter;
            if(!visited[current]){
                visited[current] = true;
                q.push(current);
                parent[current] = u;
            } 
            else if (parent[u] != current) // 已經拜訪過的點不是自己的父親 
                return true;
        }
    }
    return false;
}

bool Graph::isCyclic(){
    vector<bool> visited(V, false);	

    for(int i = 0; i < V ; i++)
        if(!visited[i] && isCyclicUtil(i, visited) )
                return true;
    return false;
}


int main() 
{ 
    Graph g1(5); 
    g1.addEdge(1, 0); 
    g1.addEdge(0, 2); 
    g1.addEdge(2, 0); 
    g1.addEdge(0, 3); 
    g1.addEdge(3, 4); 
    g1.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    Graph g2(3); 
    g2.addEdge(0, 1); 
    g2.addEdge(1, 2); 
    g2.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    return 0; 
} 
```


## 3. Disjoint Set ( 並查集 )

(1) 稍微改變上面的資料結構，變成存邊 ( 用 pair )    
[參考資料](https://www.geeksforgeeks.org/union-find/)

``` C++
#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ipair;
class Graph{
    private:
        int V;
        vector<ipair> edge;
        bool isCyclicUtil(int v, vector<bool> &visited); 
    public:
        Graph(int V):V(V){}
        void addEdge(int v, int w);
        bool isCyclic();
};

void Graph::addEdge(int v, int w){
	edge.push_back(make_pair(v, w));
}


int find(vector<int> &parent, int i) 
{ 
    if (parent[i] == -1) 
        return i; 
    return find(parent, parent[i]); 
} 
  
// A utility function to do union of two subsets  
void Union(vector<int> &parent, int x, int y) 
{ 
    int xset = find(parent, x); 
    int yset = find(parent, y); 
    if(xset!=yset){ 
       parent[xset] = yset; 
    }
} 


bool Graph::isCyclic(){
    vector<int> parent(V, -1);

    for(int i = 0 ; i < edge.size() ; i++){
        
        int x = find(parent, edge[i].first); 
        int y = find(parent, edge[i].second); 
        cout << edge[i].first << " parent "  << x << " , " << edge[i].second << " parent is "<< y << endl;
        if (x == y) return 1; 

        Union(parent, x, y);
    }

    return 0;
}


int main() 
{ 
    Graph g1(5); 
    g1.addEdge(1, 0); 
    g1.addEdge(0, 2); 
    g1.addEdge(2, 0); 
    g1.addEdge(0, 3); 
    g1.addEdge(3, 4); 
    g1.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    Graph g2(3); 
    g2.addEdge(0, 1); 
    g2.addEdge(1, 2); 
    g2.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    return 0; 
} 
```
注意這邊的做法，不是我們一般的靠左法則，這裡是靠右法則，其實靠哪裡都沒關西，最重要的是可以證明他們都要在一團就好，誰當老大都沒差。
以下是我自己寫的，是用靠左原則，也是可以行得通。

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
typedef pair<int, int> ipair;
class Graph{
    private:
        int V;
        vector<ipair> edge;
        bool isCyclicUtil(int v, vector<bool> &visited); 
    public:
        Graph(int V):V(V){}
        void addEdge(int v, int w);
        bool isCyclic();
};

void Graph::addEdge(int v, int w){
    edge.push_back(make_pair(v, w));
}


int find(vector<int> &parent, int i) 
{ 
    if (parent[i] == i) return i; 
    return find(parent, parent[i]); 
} 
  
// A utility function to do union of two subsets  
void Union(vector<int> &parent, int x, int y) 
{ 
    int xset = find(parent, x); 
    int yset = find(parent, y); 
    if(xset!=yset){ parent[yset] = xset; } // 靠誰其實都一樣 
    
    for(int i = 0 ; i < parent.size(); i++){
    	cout << parent[i] <<" ";
	}
    cout << endl;
} 


bool Graph::isCyclic(){
    vector<int> parent(V);
    for(int i = 0 ; i < V; i++) parent[i] = i;

    for(int i = 0 ; i < edge.size() ; i++){
        int x = find(parent, edge[i].first);
        int y = find(parent, edge[i].second);
        if (x == y) return 1;
        Union(parent, x, y);
    }

    return 0;
}


int main() 
{ 
    Graph g1(5); 
    g1.addEdge(1, 0); 
    g1.addEdge(0, 2); 
    g1.addEdge(2, 1); 
    g1.addEdge(0, 3); 
    g1.addEdge(3, 4); 
    g1.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    Graph g2(3); 
    g2.addEdge(0, 1); 
    g2.addEdge(1, 2); 
    g2.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    return 0; 
} 
```

輸出

    1 1 2 3 4
    1 1 1 3 4
    Graph contains cycle

    0 0 2
    0 0 0
    Graph doesn't contain cycle


但是以上的做法，`union() and find()` 時間複雜度最壞為 `O(n)`，但我們可以將其優化至`O(log(n))`    

節錄一段話
> The second optimization to naive method is `Path Compression`. The idea is to flatten the tree when find() is called. When find() is called for an element x, root of the tree is returned. The find() operation traverses up from x to find root. The idea of path compression is to make the found root as parent of x **so that we don’t have to traverse all intermediate nodes again**. If x is root of a subtree, then path (to root) from all nodes under x also compresses.

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
typedef pair<int, int> ipair;
class Graph{
    private:
        int V;
        vector<ipair> edge;
        bool isCyclicUtil(int v, vector<bool> &visited); 
    public:
        Graph(int V):V(V){}
        void addEdge(int v, int w);
        bool isCyclic();
};

void Graph::addEdge(int v, int w){
	edge.push_back(make_pair(v, w));
}


int find(vector<int> &parent, int i) 
{ 
    // 路徑壓縮將尋找路途中的點都指向老大 
    if (i != parent[i]) 
        parent[i] = find(parent, parent[i]); 
    return parent[i]; 
} 
  
// A utility function to do union of two subsets  
void Union(vector<int> &parent, vector<int> &rank, int x, int y) 
{ 
    int xset = find(parent, x); 
    int yset = find(parent, y);

    if(rank[xset] > rank[yset]){ 
        parent[yset] = xset;
    }
    else{
        parent[xset] = yset;
        if(rank[xset] == rank[yset]) rank[yset]++;
    }

}


bool Graph::isCyclic(){
    vector<int> parent(V);
    vector<int> rank(V, 0);

    for(int i = 0 ; i < V; i++) parent[i] = i;

    for(int i = 0 ; i < edge.size() ; i++){
        
        cout << "parent : ";
        for(int j = 0 ; j < parent.size(); j++){ cout << parent[j] <<" ";}
        cout << endl;
        
        cout << "rank   : ";
        for(int k= 0 ; k < rank.size(); k++){ cout << rank[k] <<" ";}
        cout << endl << endl;
        
        int x = find(parent, edge[i].first); 
        int y = find(parent, edge[i].second); 
        if (x == y) return 1; 

        Union(parent, rank, x, y);
    }

    return 0;
}


int main() 
{ 
    Graph g1(5); 
    g1.addEdge(1, 0); 
    g1.addEdge(0, 2); 
    g1.addEdge(2, 1); 
    g1.addEdge(0, 3); 
    g1.addEdge(3, 4); 
    g1.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
  	cout << endl;
  	
    Graph g2(3); 
    g2.addEdge(0, 1); 
    g2.addEdge(1, 2); 
    g2.isCyclic()? cout << "Graph contains cycle\n": 
                   cout << "Graph doesn't contain cycle\n"; 
  
    return 0; 
} 
```

輸出

    parent : 0 1 2 3 4
    rank   : 0 0 0 0 0

    parent : 0 0 2 3 4
    rank   : 1 0 0 0 0

    parent : 0 0 0 3 4
    rank   : 1 0 0 0 0

    Graph contains cycle

    parent : 0 1 2
    rank   : 0 0 0

    parent : 1 1 2
    rank   : 0 1 0

    Graph doesn't contain cycle

所以保持兩個原則 :
1. find 時需要做路徑壓縮
2. Union 需要將兩棵樹(rank 大)合併
3. 總而言之，誰是老大(rank大樹較高)就歸順誰

### 4. 著色法
[參考資料](https://www.geeksforgeeks.org/detect-cycle-direct-graph-using-colors/)

這裡是用DFS的概念，複雜度也是 DFS 的 `O(V+E)`

* 有一個 backedge 就代表有一個 cycle
* backedge : 連向祖先的邊 或是 連向自己的邊，會形成環(Cycle)
* 為了偵測 backedge，在做 DFS 時候利用 recursion stack 去持續追蹤目前點，如果我們碰到的目前點已經在 recursion stack (正在拜訪的) 內，表示有cycle的發生，表示這條邊跟祖先節點有連結，為backedge。

```C++
// A C++ Program to detect cycle in a graph 
#include<iostream> 
#include <list> 
#include <limits.h> 
  
using namespace std; 
  
class Graph 
{ 
    int V;    // No. of vertices 
    list<int> *adj;    // Pointer to an array containing adjacency lists 
    bool isCyclicUtil(int v, bool visited[], bool *rs);  // used by isCyclic() 
public: 
    Graph(int V);   // Constructor 
    void addEdge(int v, int w);   // to add an edge to graph 
    bool isCyclic();    // returns true if there is a cycle in this graph 
}; 
  
Graph::Graph(int V) 
{ 
    this->V = V; 
    adj = new list<int>[V]; 
} 
  
void Graph::addEdge(int v, int w) 
{ 
    adj[v].push_back(w); // Add w to v’s list. 
} 
  
// This function is a variation of DFSUytil() in https://www.geeksforgeeks.org/archives/18212 
bool Graph::isCyclicUtil(int v, bool visited[], bool *recStack) 
{ 
    if(visited[v] == false) 
    { 
        // Mark the current node as visited and part of recursion stack
        visited[v] = true;
        recStack[v] = true;
  
        // Recur for all the vertices adjacent to this vertex 
        list<int>::iterator i; 
        for(i = adj[v].begin(); i != adj[v].end(); ++i) 
        { 
            if ( !visited[*i] && isCyclicUtil(*i, visited, recStack) ) 
                return true; 
            else if (recStack[*i]) 
                return true; 
        } 
  
    } 
    recStack[v] = false;  // remove the vertex from recursion stack 
    return false; 
} 
  
// Returns true if the graph contains a cycle, else false. 
// This function is a variation of DFS() in https://www.geeksforgeeks.org/archives/18212 
bool Graph::isCyclic() 
{ 
    // Mark all the vertices as not visited and not part of recursion 
    // stack 
    bool *visited = new bool[V]; 
    bool *recStack = new bool[V]; 
    for(int i = 0; i < V; i++) 
    { 
        visited[i] = false; 
        recStack[i] = false; 
    } 
  
    // Call the recursive helper function to detect cycle in different 
    // DFS trees 
    for(int i = 0; i < V; i++) 
        if (isCyclicUtil(i, visited, recStack)) 
            return true; 
  
    return false; 
} 
  
int main() 
{ 
    // Create a graph given in the above diagram 
    Graph g(4); 
    g.addEdge(0, 1); 
    g.addEdge(0, 2); 
    g.addEdge(1, 2); 
    g.addEdge(2, 0); 
    g.addEdge(2, 3); 
    g.addEdge(3, 3); 
  
    if(g.isCyclic()) 
        cout << "Graph contains cycle"; 
    else
        cout << "Graph doesn't contain cycle"; 
    return 0; 
} 

```

輸出

    Graph contains cycle
