# C++ 學習筆記

> gpe : gcc 4.1.2 (c++ 98)    
> cpe : gcc 6.3.0 (c++ 14)

## 一、常見的容器(Container)
順序性容器

    1. vector
    2. list
    3. deque

關聯式容器

    1. set
    2. map
    3. multiset
    4. multimap

容器 adapter

    1. queue
    2. stack
    3. priority_queue

## 二、順序性容器

### vector

(1) 初始化(  vector 的長度可以是 variable )

一維

``` C++
    int num = 10;

    vector<int> v;      //宣告一個整數 vector

    vector<int> v(num);  //宣告一個整數 vector 且長度為 10

    int temp[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    vector<int> vec(temp, temp+10);  // 設定初始化

    vector<int> copy(vec); // 拷貝 a 容器

    vector<int> negative(10, -1); // 10 個 -1 的陣列
```

二維

``` C++
    int num = 10;

    vector< vector<int> > v(num); //宣告一個 10列 二維陣列

    vector< vector<int> > v(num, vector<int>(num)); //宣告一個 10 x 10 的二維陣列
```

(2) 迭代器

``` C++
    vector<int>:: iterator iter;
    vector<int>:: reverse_iterator r_iter; // 反向迭代器
```

正向迭代與反向迭代

正向迭代
``` C++
    1. for (iter = vec.begin(); iter != vec.end(); iter++) cout << *iter << " ";

    2. for_each(vec.begin(), vec.end(), [](int x) { cout << x << " "; }); // 需要引進 algorithm 

    3. for (int i : vec) cout << i << " ";

```

反向迭代
``` C++
    1. for (r_iter = vec.rbegin(); r_iter != vec.rend(); r_iter++) cout << *r_iter << " ";

    2. for_each(vec.rbegin(), vec.rend(), [](int x) { cout << x << " "; }); // 需要引進 algorithm 
```

iterator 也支援加法 (vector、deque 才支援)

``` C++
vector<int>::iteraor iter_half = vec.begin() + vec.size() / 2;

for (iter_half ; iter_half != vec.end(); iter_half++) cout << *iter_half << " ";
```

(3) 其他用法

``` C++

    vec.push_back(10);

    vec.clear(); // 清除所有元素

    vec.erase(p); // 移除 iterator p 指向的元素

    vec.erase(b, e); // 移除 iterator b, e 之間的元素

    vec1.swap(vec2); // 交換 vec1 vec2 元素，很快 ~

    vec.assign(n, t); // n 個值為 t 的元素

```
---
## 三、關聯式容器 

### 1. pair

``` C++
    int a[6] = {0, 1, 2, 3, 4, 5};

    pair<string, vector<int> > word_count("Lawrence", vector<int>(a, a+6));

    for(int i = 0 ; i < word_count.second.size(); i++ ) cout << word_count.second[i] << " ";

    <make_pair>

    pair<string, vector<int> > word_count_new = make_pair("Louis", vector<int>(word_count.second));

    for(int i = 0 ; i < word_count_new.second.size(); i++ ) cout << word_count_new.second[i] << " ";
```

### 關聯式容器元素是根據 key 來排序，和元素位置無關

### 2. map

(1) 初始化
``` C++
    map<int, vector<int> > word_count;
  
    word_count[0] = vector<int>(5, 10); // 直接插入
    word_count[1] = vector<int>(5, 100);

    for(int i = 0 ; i < word_count.size(); i++)
  		for(int j = 0 ;  j < word_count[i].size(); j++)
			cout << word_count[i][j] << " "; 
```

(2) 迭代器
``` C++
    map<int, vector<int> > word_count;
    map<int, vector<int> >::iterator iter;
  
    word_count[0] = vector<int>(5, 10);
    word_count[1] = vector<int>(5, 100);
    
    for(iter = word_count.begin(); iter != word_count.end(); iter++)
	    for(int i = 0 ; i < iter->second.size(); i++)
  		    cout << iter->second[i] << " ";

```

(3) 以 insert 取代下標給值，改善下標會直接賦 value 值的副作用
    insert 會返回一個 pair，如果 insert 的 key 已經存在的話，
    value 不會改變，pair 中的 pair.second = false;

``` C++
  word_count[0] = vector<int>(5, 10);
  word_count[1] = vector<int>(5, 100);
  
  for(int i = 0 ; i < word_count.size(); i++)
  		for(int j = 0 ;  j < word_count[i].size(); j++)
			cout << word_count[i][j] << " "; 
  
  cout << endl;
  pair< map<int, vector<int> >::iterator, bool> res = word_count.insert(make_pair(1, vector<int>(5, -1)));
  if(!res.second) cout << "已經存在，不做動作!" << endl; 
  // res.second = false，表示沒有 insert
    
  for(iter = word_count.begin(); iter != word_count.end(); iter++)
	for(int i = 0 ; i < iter->second.size(); i++)
  		cout << iter->second[i] << " ";
```

(4) 查詢 key - count , find
    
    cout << word_count.count(1) << endl;
    
    cout << word_count.find(2)->second << endl;

* count 只能說出現幾次(map只有 0 or 1)，沒出現返回0 

* find 可以直接指向找到的 iter，沒找到返回 end()

(5) 移除元素

``` C++
    word_count.erase(1); // remove key = 1 ，返回移除的個數
   
    word_count.erase(word_count.begin());  // remove iter = begin()

```

### 2. set
set 不會重複，和 map 操作差不多

``` C++
	vector<int> number;
	for(int i = 0 ; i < 20; i++){
		number.push_back(i);
		number.push_back(i);
	}
	cout << "vector size = " << number.size() << endl; 
	
	set<int> new_number(number.begin(), number.end());
	cout << "set size = " << new_number.size() << endl;

``` 

### 3. multiset、multimap ( 已經定義在set、map 裡面)
> 適用一對多的 key-value 問題
> multiset 就是 排序後的 vector，但是插入比較慢，記憶體站比較多
> 但是如果你要及時存取有序的話 multiset 比較好

#### multiset
``` C++
    #include <iostream>
    #include <vector>
    #include <set>
    using namespace std;
    int main()
    {
        vector<int> number;
        for(int i = 5 ; i >= 0; i--){
            number.push_back(i);
            number.push_back(i);
        }
        cout << "vector size = " << number.size() << endl; 
        
        for(int i = 0 ; i < number.size(); i++)
            cout << number[i] << " ";
        
        cout << endl << endl;

        multiset<int> new_number(number.begin(), number.end());
        cout << "set size = " << new_number.size() << endl; 
        
        for(multiset<int>::iterator iter = new_number.begin() ; iter != new_number.end(); iter++)
            cout << *iter << " ";
        
        cout << endl << endl;
    }

```
輸出

    vector size = 12
    5 5 4 4 3 3 2 2 1 1 0 0

    set size = 12
    0 0 1 1 2 2 3 3 4 4 5 5

#### multimap

新增及查詢

``` C++
    #include <iostream>
    #include <map>
    #include <string>
    using namespace std;
    int main()
    {
        multimap<string, string> tele;
        multimap<string, string>::iterator iter;
        tele.insert(make_pair("Lawrence", "0983039118"));
        tele.insert(make_pair("Lawrence", "0936262691"));
        tele.insert(make_pair("Lawrence", "0935663755"));
        tele.insert(make_pair("Louis"   , "0226917199"));


        iter = tele.find("Lawrence");
        int count_tele = tele.count("Lawrence");

        for(int i = 0 ; i < count_tele ; i++, iter++){
            cout << iter->second << endl;
        }
    }
```

使用 lower_bound、upper_bound 查詢 key 的所有 value 所構成區間

``` C++
    multimap<string, string>::iterator begin = tele.lower_bound("Lawrence"), 
                                       end   = tele.upper_bound("Lawrence");
	
    while(begin != end)
    {
        cout << begin->second << endl;
        begin++;
    }
```

或著我們也可以使用 equal_range(k)，不用 lower_bound、upper_bound，返回的是一個 pair，表示 key 所關聯元素的範圍

``` C++
    
    typedef multimap<string, string>::iterator telephone_iter;
    pair<telephone_iter, telephone_iter> pos_range = tele.equal_range("Lawrence");
	
    while(pos_range.first != pos_range.second){
        cout << pos_range.first->second << endl;
        pos_range.first++;
    }

	// 炫技簡短寫法
    while(pos_range.first != pos_range.second){
        cout << pos_range.first++->second << endl;
    }
	
    // 清晰簡短寫法
    while(pos_range.first != pos_range.second){
        cout << (pos_range.first++)->second << endl;
    }

```
---

## 容器 adapter

adapter 是使一個東西的行為向另一個東西，利用既有的容器令他的行為像另一種抽象類別，注意 stack queue 只能用 deque 容器做 adapter，vector 不行

``` C++
    #include <iostream>
    #include <stack>
    #include <queue>
    #include <deque>
    using namespace std;
    int main()
    {
        int temp[5] = {1, 2, 3, 4, 5};

        deque<int> d(temp, temp+5);

        stack<int> a_stack(d);
        cout << " stack : " ;
        while(!a_stack.empty()){
            cout << a_stack.top() << " ";
            a_stack.pop();
        }
        cout << endl;

        queue<int> a_queue(d);
        cout << " queue : " ;
        while(!a_queue.empty()){
            cout << a_queue.front() << " ";
            a_queue.pop();
        }
    }
```

輸出

    stack : 5 4 3 2 1
    queue : 1 2 3 4 5


--- 

## STL 與 container 

### find - 找容器或是陣列在搜尋區間內是否有某元素(第一個元素)

``` C++
    #include <iostream>
    #include <vector>
    #include <algorithm>
    using namespace std;
    int main()
    {
        int temp[] = {1, 2, 7, 7, 4, 5};
        vector<int> a(temp, temp+5);
        
        vector<int>::iterator iter = find(a.begin(), a.end(), 7);
        cout << *iter << endl;
        
        int *result = find(temp, temp+6, 4);
        cout << *result << endl;

    }
```
輸出

    7
    4

### accumulate - 將所有元素加總、累加，定義於 numeric 內

``` C++
    #include <iostream>
    #include <vector>
    #include <numeric>
    #include <string>
    using namespace std;
    int main()
    {
        int temp[] = {1, 2, 3, 4, 5, 6};
        vector<int> a(temp, temp+6);
            
        int sum = accumulate(a.begin(), a.end(), 0);
        cout << sum << endl;
        
        int product = accumulate(a.begin(), a.end(), 1, multiplies<int>()); // multiplies 定義在 functional STL裡面
	cout << product << endl;

        string alphabet = "abcde";
        string result = accumulate(alphabet.begin(), alphabet.end(), string(" "));
        cout << result << endl;


    }
```
輸出
    
    21
    720
     abcde

[functional相關](http://www.cplusplus.com/reference/functional/)


### find_first_of - 找出 A容器 和 B容器 第一個相同元素的位置

``` C++
    #include <algorithm>
    #include <iostream>
    #include <vector>
    
    int main()
    {	
        int tmp1[] = {0, 2, 3, 25, 5}, tmp2[] = {3, 19, 10, 2};
        std::vector<int> v(tmp1, tmp1+4);
        std::vector<int> t(tmp2, tmp2+4);
    
        std::vector<int>::iterator result = std::find_first_of(v.begin(), v.end(), t.begin(), t.end());
    
        if (result == v.end()) {
            std::cout << "no elements of v were equal to 3, 19, 10 or 2\n";
        } else {
            std::cout << "found a match at "
                    << std::distance(v.begin(), result) << "\n";
        }
    }
```
輸出

    found a match at 1

### fill / fill_n 填充一定數量的元素至容器內

#### fill
``` C++
#include <algorithm>
#include <vector>
#include <iostream>
int main()
{
    std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
 
    std::fill(v.begin(), v.end(), -1);
 
    for (auto elem : v) {
        std::cout << elem << " ";
    }
    std::cout << "\n";
}
```
輸出

    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1

#### fill_n

``` C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <iterator>
    
    int main()
    {
        std::vector<int> v1{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    
        std::fill_n(v1.begin(), 5, -1);
    
        std::copy(begin(v1), end(v1), std::ostream_iterator<int>(std::cout, " "));
        std::cout << "\n";
    }
```
輸出

    -1 -1 -1 -1 -1 5 6 7 8 9

但是 fill_n 不會做檢測

``` C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <iterator>
    
    int main()
    {
        int temp[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        std::vector<int> v1(temp, temp+10);
    
        std::fill_n(v1.begin(), 20, -1); // 填超過數量
    
        std::copy(v1.begin(), v1.end(), std::ostream_iterator<int>(std::cout, " "));
        std::cout << "\n";
    }
```

執行時會輸出

    -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
但是會停一下，表示發生錯誤


## 導入 back_inserter 

只有提供 push_back 的容器才可用，是一個 insert iterator，使用前要寫入標頭 iterator，會在尾端加入對應數量的元素

``` C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <iterator>

    int main()
    {
        int temp[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        std::vector<int> v1(temp, temp+10);
    
        std::fill_n(back_inserter(v1), 10, -1);
        
        std::copy(v1.begin(), v1.end(), std::ostream_iterator<int>(std::cout, " "));
        std::cout << "\n";
    }
```

輸出

    0 1 2 3 4 5 6 7 8 9 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1

## 演算法 _copy 版本
不更改原來的東西，使用他的複製品來運作演算法

    remove_copy
    remove_copy_if
    replace_copy
    replace_copy_if
    ...
[更多 _copy 自己找](https://zh.cppreference.com/w/cpp/algorithm)
    

### unique 消除重複元素

```C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <iterator>

    int main()
    {
        int temp[] = {0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 4, 10, 19, 5, 6, 7, 8, 9};
        std::vector<int> v1(temp, temp+20);
    
        sort(v1.begin(), v1.end());
        std::copy(v1.begin(), v1.end(), std::ostream_iterator<int>(std::cout, " "));
        std::cout << "\n";
        
        std::vector<int>::iterator end_unique = unique(v1.begin(), v1.end());
	    v1.erase(end_unique, v1.end());	 

        std::copy(v1.begin(), v1.end(), std::ostream_iterator<int>(std::cout, " "));
        std::cout << "\n";
    }
```

輸出

    0 1 2 2 2 2 2 2 2 2 2 3 4 5 6 7 8 9 10 19
    0 1 2 3 4 5 6 7 8 9 10 19

unique 會把剩餘的空間，用前面的遞補，我們要用erase 清除 從最後一個不重複的指標 到 容器尾端尖的元素


### stream iterator

### istream_iterator and ostream_iterator 

是一次性的讀取，如果初始沒給值視為 eof
[參照只能用一次的問題](https://stackoverflow.com/questions/7435713/simple-istream-iterator-question)，其實可以使用 cin.clear() 搭配 cin.sync()再輸入一次，但是前面會多一個 1  (待解決)

``` C++
    #include <vector>
    #include <iostream>
    #include <iterator>
    using namespace std;
    int main()
    {
        istream_iterator<int> in_iter(cin);
        istream_iterator<int> eof;

        vector<int> v1(in_iter, eof);
        copy(v1.begin(), v1.end(), ostream_iterator<int>(cout, " "));
    }
```

**而我們知道 iterator 將演算法和容器綁在一起**

``` C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <iterator>
    using namespace std;
    int main()
    {
        istream_iterator<int> in_iter(cin);
        istream_iterator<int> eof;

        vector<int> v1(in_iter, eof);
        copy(v1.begin(), v1.end(), ostream_iterator<int>(cout, " "));
        
        cout << flush; // 清空 ostream，不然會印到前面的 

        sort(v1.begin(), v1.end());
        unique_copy(v1.begin(), v1.end(), ostream_iterator<int>(cout, " "));
    }
```
* [清空 cout buffer](https://support.microsoft.com/en-us/help/94227/how-to-flush-the-cout-buffer-in-visual-c)


### [其他的 STL](http://www.cplusplus.com/reference/algorithm/)

* transform -  對容器每個元素做某操作，存到另一個容器中
  
* for_each - 對每個元素做操作

* random_shuffle - (C++ 98) 亂數排序
  shuffle (C++ 11)

* rotate / rotate_copy - 位移一格 

* swap - 交換兩值

* count - 容器內某值的數量
``` C++
    #include <algorithm>
    #include <vector>
    #include <iostream>
    #include <string>
    #include <iterator>
    using namespace std;

    template <typename T>
    void print(T &n){ cout << n << " "; }
    char toUpper(char &n){ return n - 32; }
    size_t toAscii(char &c){ return (size_t)c; }
    int f(int i) { return i++; }
    int main()
    {
        // for_each
        string s("hello world");
        for_each(s.begin(), s.end(), print<char>);

        cout << endl;

        //transform
        transform(s.begin(), s.end(), s.begin(), toUpper);
        for_each(s.begin(), s.end(), print<char>);

        cout << endl;

        vector<size_t> ordinals;
        transform(s.begin(), s.end(), back_inserter(ordinals), toAscii);
        for_each(ordinals.begin(), ordinals.end(), print<size_t>);

        //shuffle
        cout << endl;
        random_shuffle(ordinals.begin(), ordinals.end());
        for_each(ordinals.begin(), ordinals.end());

        cout << endl;
        //rotate
        rotate(s.begin(), s.begin() + 1, s.end());
        for_each(s.begin(), s.end(), print<char>);

        cout << endl;
        rotate(s.rbegin(), s.rbegin() + 1, s.rend());
        for_each(s.begin(), s.end(), print<char>);

    }

```

輸出

    h e l l o   w o r l d
    H E L L O   W O R L D
    72 69 76 76 79 0 87 79 82 76 68
    68 69 76 76 72 0 79 76 79 87 82
    E L L O   W O R L D H
    H E L L O   W O R L D

--- 
## 集合的運算

容器都可以用集合的運算(set、vector最常用)，下面以set為例

``` C++
#include <algorithm>
#include <set>
#include <iostream>
#include <string>
#include <iterator>
using namespace std;

template<class Iter>
void print(string title, Iter begin, Iter end){ 
	cout << title ; 
    copy(begin, end, ostream_iterator<int>(cout, " "));
	cout << endl << endl;
}

int main()
{
    set<int>::iterator iter;

    int a1[] = {1, 2,  3, 4, 5, 7, 8};
    set<int> set1(a1, a1 + sizeof(a1)/sizeof(a1[0]));
    print("set1 : ", set1.begin(), set1.end());

        
    int a2[] = {2, 3, 6, 5};
    set<int> set2(a2, a2 + sizeof(a2)/sizeof(a2[0]));
    print("set2 : ", set2.begin(), set2.end());

    cout << endl;

    set<int> intersection;
        set_intersection(set1.begin(), set1.end(),
                        set2.begin(), set2.end(),
                        inserter(intersection, intersection.begin()));
    print("intersection : " , intersection.begin(), intersection.end());


    set<int> symmetric_difference;
    set_symmetric_difference(set1.begin(), set1.end(),
                                set2.begin(), set2.end(),
                                inserter(symmetric_difference, symmetric_difference.begin()));

    print("symmetric_difference : " , symmetric_difference.begin(), symmetric_difference.end());

    set<int> unions;
    set_union(set1.begin(), set1.end(),
                set2.begin(), set2.end(),
                inserter(unions, unions.begin()));
    print("union : " , unions.begin(), unions.end());

    set<int> difference;
    set_difference(set1.begin(), set1.end(),
                    set2.begin(), set2.end(),
                    inserter(difference, difference.begin()));
    print("difference : " , difference.begin(), difference.end());       

    cout << "set1 include set2 : " << includes(set1.begin(), set1.end(), set2.begin(), set2.end()) << endl;

    cout << "set2 include set1 : " << includes(set2.begin(), set2.end(), set1.begin(), set1.end()) << endl;
}

```

輸出

    set1 : 1 2 3 4 5 7 8

    set2 : 2 3 5 6


    intersection : 2 3 5

    symmetric_difference : 1 4 6 7 8

    union : 1 2 3 4 5 6 7 8

    difference : 1 4 7 8

    set1 include set2 : 0
    set2 include set1 : 0

