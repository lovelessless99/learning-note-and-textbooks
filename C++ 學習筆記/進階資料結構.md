# C++ 進階資料結構
## 1.Kruskal Minimum Spanning Tree
[參考資料1](https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-using-stl-in-c/)

[參考資料2](https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
)

其實，如果懂了 disjoint 的 方法後，這個就非常簡單了，我這裡把 Graph 、 Disjoint 各寫成一個物件，如下

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
class Disjoint{
    private:
        int V;
        vector<int> parent;
        vector<int> rank;

    public:
        Disjoint(int v);
        int  find(int i);
        void Union(int x, int y);
};


Disjoint::Disjoint(int v):V(v){
    parent = vector<int>(V);
    rank   = vector<int>(V, 0);
    for(int i = 0 ; i < V; i++) parent[i] = i;
}

int Disjoint::find(int x){
    if(parent[x] != x)
        parent[x] = find(parent[x]);
    return parent[x];
}

void Disjoint::Union(int x, int y){
    int xset = find(x);
    int yset = find(y);

    if(rank[xset] > rank[yset])
        parent[yset] = xset;
    else{
        parent[xset] = yset;
        if(rank[xset] == rank[yset])
            rank[yset]++;
    }

}

typedef pair<int, int> ipair;
class Graph{
    private:
        int V;
        vector<pair<int, ipair> > edge;
    public:
        Graph(int V):V(V){}
        void addEdge(int u, int v, int weight);
        int kruskalMST();
};

void Graph::addEdge(int u, int v, int weight){
    edge.push_back(make_pair(weight, make_pair(u, v)));
}

int Graph::kruskalMST(){
    int mst_wt = 0;
    sort(edge.begin(), edge.end());

    Disjoint disjoint(V);

    vector< pair<int, ipair> >::iterator it;

    for (it=edge.begin(); it!=edge.end(); it++)
    {
        int u = it->second.first; 
        int v = it->second.second; 

        int set_u = disjoint.find(u); 
        int set_v = disjoint.find(v); 

        if (set_u != set_v) // 屬於不同的集合
        {
            mst_wt += it->first;
            disjoint.Union(u, v);

            cout << u << " - " << v << "         "<< it->first;
            cout << setw(10) << mst_wt << endl;
        }
    }

    return mst_wt;
}

int main() 
{ 
    Graph g(9); 
    g.addEdge(0, 1, 4); 
    g.addEdge(0, 7, 8); 
    g.addEdge(1, 2, 8); 
    g.addEdge(1, 7,11); 
    g.addEdge(2, 3, 7); 
    g.addEdge(2, 8, 2); 
    g.addEdge(2, 5, 4); 
    g.addEdge(3, 4, 9); 
    g.addEdge(3, 5,14); 
    g.addEdge(4, 5,10); 
    g.addEdge(5, 6, 2); 
    g.addEdge(6, 7, 1); 
    g.addEdge(6, 8, 6); 
    g.addEdge(7, 8, 7); 
  
    cout << "    Edges of MST are \n\n"; 
    cout << "edge     weight       累計" << endl; 
    cout << "---------------------------"<< endl;
    int mst_wt = g.kruskalMST(); 
  
    cout << "\nWeight of MST is " << mst_wt; 
    return 0;
}
```

輸出

        Edges of MST are

        edge     weight       累計
        ---------------------------
        6 - 7         1         1
        2 - 8         2         3
        5 - 6         2         5
        0 - 1         4         9
        2 - 5         4        13
        2 - 3         7        20
        0 - 7         8        28
        3 - 4         9        37

        Weight of MST is 37


## 2.Prim Minimum Spanning Tree
[參考資料1](http://alrightchiu.github.io/SecondRound/minimum-spanning-treeprims-algorithm-using-min-priority-queue.html)  

[參考資料2](https://www.geeksforgeeks.org/prims-algorithm-using-priority_queue-stl/)  

[用STL建造priority queue](https://www.geeksforgeeks.org/implement-min-heap-using-stl/)

* 不用 Union and Find
* 像 Dijkstra 的過程
* 利用 Min-Heap 加快演算法效率 ( 利用 priority queue   實作 )
* $O(ELogV)$

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
typedef pair<int, int> iPair;
class Graph{
    private:
        int V;
        list< iPair > *adj;
    public:
        Graph(int V);
        void addEdge(int u, int v, int weight);
        void primMST();
};

Graph::Graph(int V):V(V) {
    adj = new list<iPair>[V];
}

void Graph::addEdge(int u, int v, int weight){
    adj[u].push_back(make_pair(v, weight)); 
    adj[v].push_back(make_pair(u, weight)); 
}

void Graph::primMST(){
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq; 
    int src = 0;

    vector<int> key   (V, INT_MAX);
    vector<int> parent(V, -1   );
    vector<bool> inMST(V, false);

    pq.push(make_pair(0, src)); 
    key[src] = 0;

    while (!pq.empty()) 
    { 
        int u = pq.top().second; 
        pq.pop();
        
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) // 拜訪所有相鄰的點 
        {
            int v = (*i).first; 
            int weight = (*i).second;
            
            if (inMST[v] == false && key[v] > weight) // 如果沒加入 MST 且  權重比目前小 
            {
                key[v] = weight;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
    }
    for (int i = 1; i < V; ++i) 
        printf("%d - %d\n", parent[i], i);
}


int main()
{
    Graph g(9);
    g.addEdge(0, 1, 4);
    g.addEdge(0, 7, 8);
    g.addEdge(1, 2, 8);
    g.addEdge(1, 7, 11);
    g.addEdge(2, 3, 7);
    g.addEdge(2, 8, 2);
    g.addEdge(2, 5, 4);
    g.addEdge(3, 4, 9);
    g.addEdge(3, 5, 14);
    g.addEdge(4, 5, 10);
    g.addEdge(5, 6, 2);
    g.addEdge(6, 7, 1);
    g.addEdge(6, 8, 6);
    g.addEdge(7, 8, 7);
  
    g.primMST();

    return 0;
}

```
輸出

    0 - 1
    8 - 2
    2 - 3
    3 - 4
    6 - 5
    7 - 6
    6 - 7
    2 - 8

## 3. Dijkstra’s Shortest Path
[參考資料一](https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-using-priority_queue-stl/)

[參考資料二](http://alrightchiu.github.io/SecondRound/single-source-shortest-pathdijkstras-algorithm.html)

* Prim 的 key 陣列 相當於 Dijkstra 的 distance 陣列
* 和 Prim 只差在更新的地方，其他一樣
* ### 和之前一樣，利用 parent 陣列還原 subgraph


```C++
void Graph::shortestPath(int src){
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq; 

    vector<int> key   (V, INT_MAX);
    vector<int> parent(V, -1   );
   
    pq.push(make_pair(0, src));
    key[src] = 0;

    while (!pq.empty())
    {
        int u = pq.top().second;
        pq.pop();

        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) // 拜訪所有相鄰的點
        {
            int v = (*i).first;
            int weight = (*i).second;

            // 這裡是核心，更新 distance
            if ( key[v] > key[u] + weight)
            {
                key[v] = key[u] + weight;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
    }

    printf("Vertex   Distance from Source\n"); 
    for (int i = 0; i < V; ++i) 	
        printf("%d \t\t %d\n", i, key[i]); 

}
```

### 列印出最短路徑
[參考資料](https://www.geeksforgeeks.org/printing-paths-dijkstras-shortest-path-algorithm/)
* 和之前 union and find 類似，但是不用壓縮路徑
* 利用遞迴依序印出

```C++
void Graph::shortestPath(int src){
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq; 

    vector<int> key   (V, INT_MAX);
    parent = vector<int>(V, -1);


    pq.push(make_pair(0, src));
    key[src] = 0;

    while (!pq.empty())
    {
        int u = pq.top().second;
        pq.pop();

        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) // 拜訪所有相鄰的點
        {
            int v = (*i).first;
            int weight = (*i).second;

            if ( key[v] > key[u] + weight)
            {
                key[v] = key[u] + weight;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
    }

    cout << "vertex		distance	  path" << endl;
    cout << "--------------------------------------" << endl;
    for(int i = src + 1 ; i != src ; (++i)%=V, cout << endl ){
        cout << src << " -> " << i << "		" << key[i] << "		";
        printPath(i);
    }

}

void Graph::printPath(int i){
    if (parent[i] == - 1){
        cout << i; // 始源也要印
        return;
    }
    printPath(parent[i]);
    cout << i;
}
```
輸出

    vertex          distance        path
    --------------------------------------
    0 -> 1          4               01
    0 -> 2          12              012
    0 -> 3          19              0123
    0 -> 4          21              07654
    0 -> 5          11              0765
    0 -> 6          9               076
    0 -> 7          8               07
    0 -> 8          14              0128

### 可以再更好的 Dijkstra
[參考資料](https://www.geeksforgeeks.org/dijkstras-shortest-path-with-minimum-edges/)
因為一般的 Dijkstra 只會印出第一條最短路徑，但是有可能有其他走法，權重一樣但是經過的點更少。所以我們利用一個陣列
pathlength 來儲存已經走的步數

```C++
#include<bits/stdc++.h>
#include<list>
using namespace std;
typedef pair<int, int> iPair;
class Graph{
    private:
        int V;
        vector<int> parent;
        list< iPair > *adj;
    public:
        Graph(int V);
        void addEdge(int u, int v, int weight);
        void shortestPath(int src);
        void printPath(int i);
};

Graph::Graph(int V):V(V) {
    adj = new list<iPair>[V];
}

void Graph::addEdge(int u, int v, int weight){
    adj[u].push_back(make_pair(v, weight)); 
    adj[v].push_back(make_pair(u, weight)); 
}

void Graph::shortestPath(int src){
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq; 

    vector<int> key   (V, INT_MAX);
    parent = vector<int>(V, -1);
    vector<int>pathlength(V, 0);

    pq.push(make_pair(0, src));
    key[src] = 0;

    while (!pq.empty())
    {
        int u = pq.top().second;
        pq.pop();

        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            int v = (*i).first; 
            int weight = (*i).second;

            if ( key[v] > key[u] + weight) 
            {
                parent[v] = u;
                pathlength[v] = pathlength[parent[v]] + 1;

                key[v] = key[u] + weight;
                pq.push(make_pair(key[v], v));
            }

            else if(  key[v] == key[u] + weight && pathlength[u] + 1 < pathlength[v]){
                parent[v] = u; 
                pathlength[v] = pathlength[u] + 1;
            }
        }
    }

    cout << "vertex		distance	  path" << endl;
    cout << "--------------------------------------" << endl;
    for(int i = src + 1 ; i != src ; (++i)%=V, cout << endl ){
        cout << src << " -> " << i << "		" << key[i] << "		";
        printPath(i);
    } 

}

void Graph::printPath(int i){
    if (parent[i] == - 1){ 
        cout << i;
        return; 
    }
    printPath(parent[i]); 
    cout << i;
}

int main() 
{ 
    Graph g(5); 
    g.addEdge(0, 1, 1); 
    g.addEdge(0, 4, 10); 
    g.addEdge(1, 2, 4); 
    g.addEdge(2, 3, 7); 
    g.addEdge(4, 3, 2); 


    g.shortestPath(0); // 0 到 各點的最短路徑 

    return 0; 
} 

```

輸出

    vertex          distance        path
    --------------------------------------
    0 -> 1          1               01
    0 -> 2          5               012
    0 -> 3          12              043
    0 -> 4          10              04