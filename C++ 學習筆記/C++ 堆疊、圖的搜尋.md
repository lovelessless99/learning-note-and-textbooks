# C++ 資料結構篇
## 1. 堆積 (Heap)
[參考資料](https://elloop.github.io/c++/2016-11-29/learning-using-stl-72-make-heap)

STL 的 make_heap 可以利用容器幫助做 max_heap

```C++
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int a[] = {1, 2, 3, 4, 5, 6};
    vector<int> heap(a, a+6);
    make_heap(heap.begin(), heap.end());	

    for(int i = 0 ; i < 6; i++)
        cout << heap[i] << " ";
} 
```

輸出

    6 5 3 4 2 1

如果想要 min heap，需要用到自己定義的 comparator，這裡利用`greater<int>()` 取代預設的`less<int>()`，當然也可以用自己定義的做

```C++
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int a[] = {1, 2, 3, 4, 5, 6};
    vector<int> heap(a, a+6);
    make_heap(heap.begin(), heap.end(), greater<int>());	

    for(int i = 0 ; i < 6; i++)
        cout << heap[i] << " ";
} 
```

輸出

    1 2 3 4 5 6

新增元素時，先將容器新增元素，再用 push_heap 調整 heap

```C++
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int a[] = {1, 2, 3, 4, 5, 6};
    vector<int> heap(a, a+6);
    make_heap(heap.begin(), heap.end(), greater<int>());	

    cout << "Initial : " ;
    for(int i = 0 ; i < heap.size(); i++)
        cout << heap[i] << " ";


    cout << endl << endl << "New 300 : ";
    heap.push_back(300);

    push_heap(heap.begin(), heap.end(), greater<int>());

    for(int i = 0 ; i < heap.size(); i++)
        cout << heap[i] << " ";
		
} 
```
注意，一開始如果用 min_heap，後面做新增刪除的動作都要加入比較子

輸出

    Initial : 1 2 3 4 5 6

    New 300 : 1 2 3 4 5 6 300

而刪除的話，就如 heap 算法一樣，把頂端元素放在最後面取出後刪除。並把最後一個元素放到第一個

```C++
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    int a[] = {1, 2, 3, 4, 5, 6};
    vector<int> heap(a, a+6);
    make_heap(heap.begin(), heap.end(), greater<int>());	

    cout << "Initial : " ;
    for(int i = 0 ; i < heap.size(); i++)
        cout << heap[i] << " ";
        
    cout << endl << endl << "get minimize : ";
    pop_heap(heap.begin(), heap.end(), greater<int>());
    for(int i = 0 ; i < heap.size(); i++)
        cout << heap[i] << " ";
    cout << endl;
    heap.pop_back();

    cout << endl << "after get minimize : ";
    for(int i = 0 ; i < heap.size(); i++)
        cout << heap[i] << " ";
		
} 

```

輸出

    Initial : 1 2 3 4 5 6

    get minimize : 2 4 3 6 5 1

    after get minimize : 2 4 3 6 5

剩下就去參考該網站，當然也可以用 queue 去實作

[參考網站](https://www.fluentcpp.com/2018/03/20/heaps-and-priority-queues-in-c-part-3-queues-and-priority-queues/)

## 這邊很重要注意一點，`<bits/stdc++.h>` 是一個很聰明的作法，把所有標準庫都囊括進來了。
在使用GNU GCC Compiler的時候，你可以包含一個頭文件<bits/stdc++.h>，便可以使用C++中的各種標準庫，而不用一個一個包含進來。
這在acm比賽中是一種常用的做法。



## 2. Graph
無權重的邊[參考網站](https://www.geeksforgeeks.org/graph-implementation-using-stl-for-competitive-programming-set-1-dfs-of-unweighted-and-undirected/) 

```C++
// A simple representation of graph using STL, 
// for the purpose of competitive programming 
#include<bits/stdc++.h> 
using namespace std; 
  
// A utility function to add an edge in an 
// undirected graph. 
void addEdge(vector<int> adj[], int u, int v) 
{ 
    adj[u].push_back(v); 
    adj[v].push_back(u); 
} 
  
// A utility function to do DFS of graph 
// recursively from a given vertex u. 
void DFSUtil(int u, vector<int> adj[], 
                    vector<bool> &visited) 
{ 
    visited[u] = true; 
    cout << u << " "; 
    for (int i=0; i<adj[u].size(); i++) 
        if (visited[adj[u][i]] == false) 
            DFSUtil(adj[u][i], adj, visited); 
} 
  
// This function does DFSUtil() for all  
// unvisited vertices. 
void DFS(vector<int> adj[], int V) 
{ 
    vector<bool> visited(V, false); 
    for (int u=0; u<V; u++) 
        if (visited[u] == false) 
            DFSUtil(u, adj, visited); 
} 
  
// Driver code 
int main() 
{ 
    int V = 5; 
  
    // The below line may not work on all 
    // compilers.  If it does not work on 
    // your compiler, please replace it with 
    // following 
    // vector<int> *adj = new vector<int>[V]; 
    vector<int> adj[V]; 
  
    // Vertex numbers should be from 0 to 4. 
    addEdge(adj, 0, 1); 
    addEdge(adj, 0, 4); 
    addEdge(adj, 1, 2); 
    addEdge(adj, 1, 3); 
    addEdge(adj, 1, 4); 
    addEdge(adj, 2, 3); 
    addEdge(adj, 3, 4); 
    DFS(adj, V); 
    return 0; 
} 

```

### 有權重的邊
[參考資料](https://www.geeksforgeeks.org/graph-implementation-using-stl-for-competitive-programming-set-2-weighted-graph/)

```C++
#include <bits/stdc++.h> 
using namespace std; 
  
// To add an edge 
void addEdge(vector <pair<int, int> > adj[], int u, 
                                     int v, int wt) 
{ 
    adj[u].push_back(make_pair(v, wt)); 
    adj[v].push_back(make_pair(u, wt)); 
} 
  
// Print adjacency list representaion ot graph 
void printGraph(vector<pair<int,int> > adj[], int V) 
{ 
    int v, w; 
    for (int u = 0; u < V; u++) 
    { 
        cout << "Node " << u << " makes an edge with \n"; 
        for (auto it = adj[u].begin(); it!=adj[u].end(); it++) 
        { 
            v = it->first; 
            w = it->second; 
            cout << "\tNode " << v << " with edge weight ="
                 << w << "\n"; 
        } 
        cout << "\n"; 
    } 
} 
  
// Driver code 
int main() 
{ 
    int V = 5; 
    vector<pair<int, int> > adj[V]; 
    addEdge(adj, 0, 1, 10); 
    addEdge(adj, 0, 4, 20); 
    addEdge(adj, 1, 2, 30); 
    addEdge(adj, 1, 3, 40); 
    addEdge(adj, 1, 4, 50); 
    addEdge(adj, 2, 3, 60); 
    addEdge(adj, 3, 4, 70); 
    printGraph(adj, V); 
    return 0; 
} 
```

## BFS 廣度優先搜尋
[參考資料](https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/)

```C++
#include<bits/stdc++.h>
#include <list> 
  
using namespace std; 
class Graph{
    int V; 
    list<int> *adj;

    public:
        Graph(int V);
        void addEdge(int v, int w);
        void BFS(int start);
};

Graph::Graph(int V):V(V), adj(new list<int>[V]){ 

}

void Graph::addEdge(int v, int w) 
{ 
    adj[v].push_back(w); // Add w to v’s list. (因為是有向圖，所以不用雙向)
} 
  
void Graph::BFS(int s) 
{ 
    // Mark all the vertices as not visited 
    bool *visited = new bool[V]; 
    for(int i = 0; i < V; i++) 
        visited[i] = false; 
  
    // Create a queue for BFS 
    list<int> queue; 
  
    // Mark the current node as visited and enqueue it 
    visited[s] = true; 
    queue.push_back(s); 
  
    // 'i' will be used to get all adjacent 
    // vertices of a vertex 
    list<int>::iterator i; 
  
    while(!queue.empty()) 
    { 
        // Dequeue a vertex from queue and print it 
        s = queue.front(); 
        cout << s << " "; 
        queue.pop_front(); 
  
        // Get all adjacent vertices of the dequeued 
        // vertex s. If a adjacent has not been visited,  
        // then mark it visited and enqueue it 
        for (i = adj[s].begin(); i != adj[s].end(); ++i) 
        { 
            if (!visited[*i]) 
            { 
                visited[*i] = true; 
                queue.push_back(*i); 
            } 
        } 
    } 
} 



// Driver program to test methods of graph class 
int main() 
{ 
    // Create a graph given in the above diagram 
    Graph g(4); 
    g.addEdge(0, 1); 
    g.addEdge(0, 2); 
    g.addEdge(1, 2); 
    g.addEdge(2, 0); 
    g.addEdge(2, 3); 
    g.addEdge(3, 3); 
  
    cout << "Following is Breadth First Traversal "
         << "(starting from vertex 2) \n"; 
    g.BFS(2); 
  
    return 0; 
} 
```
### [BFS 相關應用](https://www.geeksforgeeks.org/applications-of-breadth-first-traversal/)