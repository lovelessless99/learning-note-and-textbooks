# CPE 學習筆記- 未作的筆記
1. 容器使用方法、容器排序 
2. 常用的標準函式庫
3. algorithm sstream bitset
4. 常用的資料結構(圖、樹、堆積) 用 STL makeheap... 或是其他容器實現
5. 字串處理、分割、...

6. [algorithm](http://www.cplusplus.com/reference/algorithm/)
7. 字串、數字、二進位
8. 餘數、輾轉相除法
9. cin.ignore()
10. map sort by key / sort by value/ sort by key and value
11. 字元轉字串(String(1, 'A'))、字元轉數字、字串分割
12. 動態分配陣列大小 int[num] 不能用
13. 整數範圍、long 整數範圍、 double 範圍
14. 控制輸出格式字串 setw conio

15. stoi atoi...
16. int to binary, int to string, binary to int, binary to base n ...

17. 尋找質數、大整數整除
18. 大整數換成 n_based 問題
## algorithm  (注意 gcc 的版本是否支援)

      1. copy_if  // filter
      2. fill_n
      3. find
      4. int i = std::stoi("01000101", nullptr, 2);
      5. ostream_iterator
      6. transform_reduce
      7. reduce // numeric
      8. reverse
      9. make_heap ******
    
[algorithm 相關的資源](https://zh.cppreference.com/w/cpp/algorithm)

* gpe gcc 4.2.1 ( C++ 98)
* cpe gcc 6.3.0 ( C++ 14)
* [C++ 98 C++ 11 差異](https://www.cnblogs.com/harlanc/p/6504431.html)
* 版