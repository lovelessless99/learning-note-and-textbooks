# CPE 常用技巧
## 1. 歐幾里得演算法(輾轉相除)

```C++
int GCD(int a, int b){
	while(b){
		int r = a%b;
		a = b;	
		b = r;
	}
	return a;
}
```
或是更簡短的

    a = 12, b = 8
    a = 12%8 = 4, swap(a=4, b=8);
    a = 8%4 = 0, 跳出迴圈
    return b = 4;

```C++
int GCD(int a, int b){
    while(a%=b){ swap(a, b); }
    return b;
}
```
## 2. 找質數
(1) 直接先找合成數
```C++
int com[1000000] = {0};
for(int i = 2 ;  i < 1000; i++){
    if(com[i]) continue;
    for(int j = i*2; j < 1000000; j+=i) com[j] = 1;
}
```

(2) 判斷是否質數

```C++
bool prime(int n){
    for(int i = 2 ; i < n; i++) if( n % i == 0) return false;
    return true;
}
```
## 3. 動態分配陣列大小
int[num]在執行期間決定陣列大小是不允許的，只有擴充版的編譯器材可以接受，十分危險的寫法。 

```C++
int n = 10;
double* a = new double[n]; // Don't forget to delete [] a; when you're done!

int n = 10;
std::vector<double> a(n); // Don't forget to #include <vector>
```
## 4. 二維陣列用 map
用map帶替二維陣列的好處是不用怕記憶體超標的問題，而且value會自動初始化為0。不用怕亂碼。

```C++
map<int, map<int, char>> twoDarray;
```

## 5. 善用 macro
使用情境 : 很常判斷陣列某位置是否為 0 

```C++
#define prime(x) (np[x]==0)
// 可取代一直寫 np[x]==0 的混亂
```

## 6. 控制輸出字串格式
「setprecision」包含在`<iomanip>`函式庫內，與 setw 不同的是， setprecision 控制的是輸出數值的「位數」，而 setw 控制的則是輸出數值或字串的「寬度」，如果只控制「小數點後的位數」，加個 `fixed` 即可

```C++
#include<iomanip>
cout  <<  setprecision(2)  << 3.1111111  <<  endl; // 3.1

//只控制「小數點後的位數」
cout  << fixed  <<  setprecision(2) << 3.1234  <<  endl; // 3.12
```

## 7. 排序比較子 comparator of sort 
注意 algorithm 的 比較子只有循序容器可用，map、set都不行 ( 但map set 都已照key順序排)

升序(前面比後面小) Ex: 1 2 3 4 5

``` C++
bool cmp(int a, int b){
    return a < b;
}
```

降序(前面比後面大) Ex: 5 4 3 2 1
``` C++
bool cmp(int a, int b){
    return a < b;
}
```

pair 照 value 再照 key 排序 (只能用`vector<pair<T, V> >`)

``` C++
bool cmp(pair<char, int> a, pair<char, int> b){
    return a.second != b.second?  a.second < b.second : a.first > b.first; // value 一樣比 key(降)，不同比value(升) 
}

map<char, int> frequency; map<char, int>::iterator iter;	
for(int i = 0 ; i < sentence.size(); i++) frequency[sentence[i]] += 1;
    
vector<pair<char, int> > sort_frequency(frequency.begin(), frequency.end()); 	
sort(sort_frequency.begin(), sort_frequency.end(), cmp);	//map sort by value then sort by key

```
這邊把 map 給 vector 的 拷貝建構技巧也很重要。
當然，也可以泛型，但**務必要記得sort時要給參數型態，不然會錯。**`Ex: cmp<char, int>`

```C++
template<class T, class V> 
bool cmp(pair<T, V> a, pair<T, V> b){
    return a.second != b.second?  a.second < b.second : a.first > b.first; // value 一樣比 key(降)，不同比value(升) 
}
map<char, int> frequency; map<char, int>::iterator iter;	
for(int i = 0 ; i < sentence.size(); i++) frequency[sentence[i]] += 1;
    
vector<pair<char, int> > sort_frequency(frequency.begin(), frequency.end());
sort(sort_frequency.begin(), sort_frequency.end(), cmp<char, int>)
```
---- 
## 其他資源

[確定是否奇數](https://www.geeksforgeeks.org/3-ways-check-number-odd-even-without-using-modulus-operator-set-2-can-merge-httpswww-geeksforgeeks-orgcheck-whether-given-number-even-odd/)