# CPE常用技巧 - 整數、大整數與字串基底之轉換運算

## 一、基底的轉換

(1) 整數轉二進位
#### 法一、使用stack 容器adapter

```C++
#include <iostream>
#include <stack> 
using namespace std;
int main()
{ 	
    stack<int> binary;
    int num = 12, rem;

    while(num){
        rem = num % 2;
        num /= 2;
        binary.push(rem);
    }

    int size = binary.size();
    for(int i = 0 ; i < size; i++, binary.pop() ){
        cout << binary.top();
    }
}

```

#### 法二、把迴圈算出的結果存成 String 再反轉
Ex : 12

12 / 2 = 6 .... 0    
6  / 2 = 3 .... 0   
3  / 2 = 1 .... 1   
1  / 2 = 0 .... 1   
後面都是 (0 / 2 = 0 .... 0)     
每次迭代的下一次被除數是上衣次迭代的商數，所以到商數為 0 就停止， $12 = (1100)_2$
```C++
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;
int main()
{ 	
    int num = 12, rem;
    stringstream stream;

    while(num){
        rem = num % 2;
        num /= 2;
        stream << rem;
    }

    string result = stream.str();
    reverse(result.begin(), result.end());
    cout << result;
}

```

#### 法三、使用位移運算子

Ex : 12 , initial mask = 32 (10000)

12 & 32 = 1100 & 100000 = 000000 = 0, mask >>= 1, mask = 16   

12 & 16 = 01100 & 10000 = 00000  = 0, mask >>= 1, mask = 8

12 & 8  = 1100 & 1000 = 1000 = 8, mask >>= 1, mask = 4

12 & 4 = 1100 & 100 = 0100 = 4, mask >>= 1, mask = 2

12 & 2 = 1100 & 10 = 00 = 0, mask >>= 1, mask = 1

12 & 1 = 1100 & 1 = 0, mask >>= 1, mask = 0


```C++
#include <iostream>
using namespace std;

void Convert(unsigned int val)
{
    //int shift_bit = (sizeof(int) * 8 - 1);
    int shift_bit = 5;
    unsigned int mask = 1 << shift_bit;

    for(int i = 0; i <= shift_bit; i++, mask >>= 1)
        cout << ( (val & mask)? '1' : '0' ) ;
    // 使用三元運算子記得要注意符號優先序 
}

int main()
{
    Convert(12);
}
```

#### 法三、使用 `<bitset>` 函式庫
bitset<bits數>(數字)，注意bitset大小只能常數

```C++
#include <iostream>
#include <bitset>
using namespace std;

int main()
{
    string s = bitset< 64 >( 12 ).to_string();
    s = s.erase(0, s.find("1")); //刪掉第一個 1 之前的0
    cout << s << endl;
}

```

[其他相關操作請參考這裡](http://edisonx.pixnet.net/blog/post/34045379-bitset-%E6%95%B4%E7%90%86)    

也可以用字串初始化
```C++
#include <iostream>
#include <bitset>
using namespace std;

int main()
{
    string s1 = bitset< 64 >( string("0001101") ).to_string(); //字串初始化 

    bitset<64> input; cin >> input; // 要輸入二進位 
    unsigned long k = input.to_ulong(); // 輸出十進位的值 
}
```

(2) 大整數字串轉二進位



