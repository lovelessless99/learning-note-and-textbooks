# Python Virtual Envirnment 創建方法
> 如果想在資料夾內創造虛擬環境，不需要安裝Anaconda
> ，因為 Anaconda 有夠肥大，而且他的管理套件 GUI 有一點慢(可能是我的電腦效能不好 冏)。因此我們可以利用等等介紹的兩種方法，去創建獨立的虛擬環境，以利之後Python 程式之開發及利用。 

## 一、利用 Virtualenv 套件
首先，先安裝 Virtualenv 套件

    pip install Virtualenv

然後創建一個資料夾，或是進去你的專案目錄，這裡我先在**桌面創建一個 VirtualTest** 資料夾做測試，創建好後，利用 Command Line，將當前目錄進到 VirtualTest

    cd Desktop/VirtualTest

進去之後，輸入以下命令，我將虛擬環境的資料夾取名為  `myvenv`，當然你也可以取其他名字也是隨意

    virtualenv myvenv

你就可以看到你的資料夾裡面多出 myvenv 的資料夾
那就恭喜你，成功一半！

再來，將當前目錄 ( 現在在VirtualTest資料夾內 ) 進到裡面的資料夾 ` myvenv/Scripts/`

     cd myvenv/Scripts

執行

    activate

你可以看到你的Command Line 前面多了 `(myvenv)`，表示你現在在虛擬環境裡面了。恭喜你😆 。另外，因為我方才建立的虛擬環境名稱是myvenv，所以顯示的是你建立時的名字。  

再來測試一下，先隨便安裝兩個套件，我這裡安裝`Markdown` 還有 `requests`

    pip install Markdown
    pip install requests

再用 freeze 套件看目前的安裝套件，輸入

    pip freeze

可以看到顯示剛剛安裝的兩個套件，還有他們的相依套件
`pip freeze` 會列出所有被安裝的 python 模組及其版本
我們將其**輸出**到一個檔案 `requirements.txt`(這個檔案名稱算是約定俗成(待確認))

    pip freeze > requirements.txt

你可以看到你目前的檔案目錄底下，多出 `requirements.txt`的檔案，裡面的內容就是我們剛剛用 `pip freeze` 在 console 看到的安裝過的套件文字

### 補充 : 為什麼要 requirements.txt ?

因為 `requirements.txt` 記載著這個虛擬環境安裝的套件及其版本號，將來如果想重現現在的環境，可不需要一次一次 `pip install` ...，十分繁瑣又超級麻煩的。可直接使用以下指令

    pip install -r requirements.txt

即可一次安裝全部的套件，有夠方便的哈哈😆。
將來如果想創建和這個一模一樣的虛擬環境，不用再一直打`pip install` 的指令。直接用此方法即可。

但要注意的一點是，每新安裝一個套件，就要用`pip freeze > requirements.txt`更新一次內容，才可和現有環境同步。

另外，有時候 `requirements.txt` 套件太舊，你想更新到最新版，可在後面加入 `--upgrade`，如下

    pip install -r requirements.txt --upgrade

即可將所有當前套件安裝以最新版的套件安裝。

----

## 二、 使用 pipenv 套件

> 利用 `virtualenv` 的方法，有幾個的缺點 :
> 
> * 每次更新或是安裝新的套件，必須再順便輸指令手動更新  `requirements.txt` 的檔案，當然你如果沒有要創造相同的環境，你也可以不用做。所以維護更新 `requirements.txt` 卻是一件麻煩的事情，需要透過手動來維護
>
> * 不容易知道套件之間的相依關係，像是剛剛安裝的 `requests` 套件，安裝時也會自動安裝相依的套件，但是當你用 `pip freeze` 或是直接去已經更新過後的最新版 `requirements.txt`，你會發現都沒有一個階層性，就是說你不知道誰相依誰，上面給你的內容只有所有的套件，非常的不清楚。
 
`pipenv` 則是目前 Python 官方所推薦的套件管理工具，改善了上述方法的缺點。

首先，先安裝 `pipenv` 套件

    pip install pipenv

再來如同上述方法，進入專案資料夾，輸入
    
    pipenv --three

後面的 `three` 表示你是安裝 `Python 3.x` 的環境。你會發現資料夾內多出一個`Pipfile`的檔案，這個檔案是記錄你的虛擬環境安裝的套件。

**但是有些人會有個問題，怎麼沒有虛擬環境的資料夾!?😭😭**

這個時候，你在命令提示字元( 已經進入到該目錄 )後，輸入 
    
    pipenv --venv

可以知道你的虛擬環境在哪裏，像我的電腦就是預設在
`C:\Users\ASUS\.virtualenvs`底下，有個`專案資料夾名稱-Hash值`的資料夾名稱，例如你今天`Pipfile`是位在 `MyVenvTest` 資料夾底下，你創建後的虛擬環境名稱資料夾就是 `MyVenvTest-IKRvLRx9`。
後面是雜湊碼，所以虛擬環境名稱你不用自己設，系統都幫你設好了。

**但是這樣很麻煩ㄟ，為什麼不像之前一樣都虛擬環境資料夾都安裝在專案資料夾裡面😡😡😡，還要去那個資料夾找所有的套件檔案**

所以，如果你想要像我一樣，不希望虛擬環境安裝用系統預設的路徑，在 `Command Line` 依照你的作業系統輸入以下指令設定改變環境變數

    <Windows>
    set PIPENV_VENV_IN_PROJECT="enabled"

    <Linux>
    export PIPENV_VENV_IN_PROJECT="enabled"

>[ 2019-01-04 Update]  
>會發現，下指令設好的環境變數，重開機會重置
>你可能要重新再輸入一次上面的指令，或是你想要一勞永逸，直接去系統設定環境變數，變數名稱是  `PIPENV_VENV_IN_PROJECT` ，變數值是 `"enabled"`，**重新開機後(重新開機讓系統重新載入環境變數)**，之後虛擬環境就可以都在你的資料夾內囉😆😆😆

如此一來，虛擬環境就可以在你的資料夾內囉 ~~😆😆😆，你會發現資料夾內多一個`.venv`虛擬環境資料夾，而啟動的虛擬環境名稱就會跟你的專案資料夾名稱一樣。而裡面有一個`.project`檔案，裡面的內容是你專案的路徑。

啟動環境方式也是跟之前一樣，到`.venv\Scripts`執行`activate`，即可啟動虛擬環境

啟動虛擬環境後，這裡安裝套件的方法和一般稍微不同。利用

    pipenv install markdown
    pipenv install requests

>[ 2019-01-04 Update]  
>其實不用進入虛擬環境內，你直接當前目錄在專案內執行上面兩個指令即可

安裝兩個套件後，你再用文字編輯器看`Pipfile`，你可以發現他自動更新，不用像之前`requirement.txt`一樣要手動更新維護，超級無敵方便的啦😆😆😆

另外，你看另外一個`Pipfile.lock`檔案，可以從上面看到除了 `request` 之外，它同時記錄 `request` 所需相依套件的版本，這是用來確保在任何地方透過這份檔案安裝套件都是相同的套件（相較之外 `requirements.txt` 則無法確保所安裝的套件是一樣的。）

然後，你再輸入

    pipenv graph

你可以看到命令提示字元輸出

    Markdown==3.0.1
    requests==2.21.0
        - certifi [required: >=2017.4.17, installed: 2018.11.29]
        - chardet [required: >=3.0.2,<3.1.0, installed: 3.0.4]
        - idna [required: >=2.5,<2.9, installed: 2.8]
        - urllib3 [required: >=1.21.1,<1.25, installed: 1.24.1]

你一看就知道 `requests` 相依那些套件，整個架構清晰明瞭，安裝此套件會順便安裝`certifi` `chardet` `idna` `urllib3`，主從式的階層關系一目了然，不像之前的方法`pip freeze`有用到的套件全部一起輸出，根本不知道誰相依誰。

另外，`Pipenv` 還有一個好處，通常有一些 Python 包只在你的開發環境中需要，而不是在生產環境中，例如單元測試包。 `Pipenv` 使用 `--dev` 後綴去區分兩個環境。可以區分 dev 環境的套件，在安裝時可以選擇要不要包含 dev 環境的套件．就可以保持 prod 套件的乾淨。

    pipenv install pandas --dev

你就只會在 dev 環境安裝 `pandas`，你在`Pipfile.lock` 或是 `Pipfile`可以看到兩者套件的區分。所以之後別人要複製時有些套件只是要測試使用時就可以以此區隔。

所以，其實你在使用這個虛擬環境時，不管是 dev 或是 prod 的套件你都可以用喔！它很聰明的用 `Pipfile` `Pipfile.lock` 記下誰是開發用的，誰是生產用的，如此一來，下一個人(或是你)如果創造的新的虛擬環境要跟你一樣，但是他可以決定要不要安裝開發用的套件(待會就會看到)

前面講得可能有點亂，舉個例子，目前你會發現`Pipfile`的內容

    [[source]]
    name = "pypi"
    url = "https://pypi.org/simple"
    verify_ssl = true

    [dev-packages]
    markdown = "*"
    numpy = "*"
    pandas = "*"

    [packages]
    markdown = "*"
    requests = "*"
    numpy = "*"

    [requires]
    python_version = "3.6"

當你今天創了另外一個新的(生產)環境時，你如果想要複製當前環境，你只要複製之前的`Pipfile`檔案到新環境
目錄底下，執行

    pipenv install

即可以安裝所有 `Pipfile` 內 `[packages]`的套件，可以避免有些只是要在開發時用的套件也一起裝進來，可以使套件比較乾淨，這個時候你可以用`pipenv graph` 驗證我說的話
    
    Markdown==3.0.1
    numpy==1.15.4
    requests==2.21.0
        - certifi [required: >=2017.4.17, installed: 2018.11.29]
        - chardet [required: >=3.0.2,<3.1.0, installed: 3.0.4]
        - idna [required: >=2.5,<2.9, installed: 2.8]
        - urllib3 [required: >=1.21.1,<1.25, installed: 1.24.1]

發現真的只有裝`package`的部分而沒有裝`[dev-packages]`的部分

那如果今天是你的共同開發者，想要一起引進`dev`環境的套件只需要在後面加入`--dev`即可

    pipenv install --dev

>所以用 pipenv 好處真的超多的😄😄😄

最後，如果你還是想要輸出`requirements.txt`，就輸入

    pipenv lock --requirements > requirements.txt

或是

    pipenv lock -r > requirements.txt

你會發現，是生產環境的套件而不是開發環境的，如果你也要連開發環境的套件一起加進去，和上面一樣，加入`--dev`即可

    pipenv lock --requirements > requirements.txt --dev

或是

    pipenv lock -r > requirements.txt --dev

如此一來，所有的套件內容都在檔案內


最後，可加入`.env`檔，這樣使用虛擬環境的shell

    pipenv shell

時可以自動載入環境變數，非常方便。但這裡我還沒有深究就是。

----
## 結論 : 使用 pipenv 的優點
>使用 `virtualenv` & `requirements.txt` 確實解決了一些套件管理問題，也可以讓別人也可以快速安裝專案所需要的套件，然而維護更新 `requirements.txt` 卻是一件麻煩的事情，需要透過手動來維護，不僅如此，當我們套件有分 `development` & `production` 不同的套件時，我們可能還需要特定有個 `requirements-dev.txt` 來區分兩個不同環境的套件

>初學者應該都有類似的經驗，當初剛學 python，什麼東西都亂安裝，一台 mac 理可能用了 brew install ，又用了 anaconda，安裝套件有時候 pip，又有 pip3 又有 conda install，有時候還要自己編譯．偶爾還發生，明明就有裝，但是 import 的時候卻找不到套件，後來才發現原來是該目錄沒有加入 path 等諸如此類的鳥問題

* 安裝 / 管理 優勢

    * 相較 `pip `是按順序安裝， `pipenv` 是使用並行（parallel）同步安裝套件，能夠提升安裝套件的速度    
  
    * 自動產生與更新`Pipfile and Pipfile.lock` 解決了維護 `requirements.txt` 的問題。

    * 自動管理套件之間的 `dependency` ，方便移除套件．例如安裝 `pyspark` 時會需要一併安裝 `py4j` ，如果單純使用 `pip` 安裝，雖然可以一併安裝，但是用 `pip uninstall` 移除時需要一個一個移除．這點在 `pipenv` 會幫忙管理，自動移除相關的 lib．

    * 產生 `dependency` 的樹狀圖 (e.g. $ `pipenv graph`)


* 區分 `dev` 和 `prod` 環境的優勢
    * 可以區分 `dev` 環境的套件，在安裝時可以選擇要不要包含 `dev` 環境的套件．就可以保持 `prod` 套件的乾淨


* `.env 整合` : 我們在開發程式的時候常常會有許多的環境變數要設定，只要用 `pipenv` 你可以在你的專案目錄底下新增一個如下的 `.env` 檔案，就會自動幫你在用 `pipenv` 執行指令時的環境變數設定好。可以透過 `.env` 自動載入不同環境變數


* 透過套件的 Hash 安全性檢查確認 （當安裝套件 hash 值不同時，跳出錯誤，防止惡意套件侵入）

* 集成了 `virtualenv、pyenv、pip` 三者的功能於一身，類似於 `php` 中的 `composer`


### **以後可以的話，盡量用 pipenv 創造虛擬環境吧 😆😆😆😆，Go Go Go ~~~**
----
## 後記 
 我一開始有使用過 Python 內建的

    python -m venv myvenv

創建虛擬環境，但是當安裝時會發生問題，他是 global 的安裝，不是 local，意思是裝在你的系統本身，而不會裝在你的虛擬環境，這個地方我至今仍未解決，不過即使解決了，還是要面臨像第一種方法一樣`requirement.txt`的維護問題，以及開發環境和生產環境的區分問題，所以我們還是直接使用第二個方法就好。

## 後記 2
1. 每次 `pipenv install flask -skip-lock` 比較快，不用解 lock
2. `pipenv --python 3.10` 可以指定 python 版本 