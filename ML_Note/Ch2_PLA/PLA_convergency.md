# Percetron Learning Algorithm Convergency Proof

## 一、證明精神描述
### 1. 《參數說明》  
>$t$ : 演算法結束的迭代次數  
>$w_t$ : 演算法第 $t$ 輪迭代停止時的權重向量  
>$w_f$ : 目標函數 $f$ 的權重向量

### 2. 《演算法目的》  
>經過 $t$ 輪迭代，找到和 $w_f$ 無限接近的 $w_t$，即 $g(w_t) \approx f(w_f)$ 並且 $t$ 需要收斂，不可以無限次迭代，使得演算法停不下來。 

### 3. 《證明方法》  
>兩個向量 $w_t$ 、 $w_f$ 無限接近，即相似。通常使用內積 $w_t \cdot w_f$表示相近的程度，內積越大，兩者越相似

### 4. 《存在問題》
> 內積 $w_t \cdot w_f$ 增大有兩種可能 :  
> * 兩向量夾角越小
> * 向量本身長度越長，需要排除此因素，所以需要求兩個向量 **單位化** 後的內積(即單位向量內積)，此時內積的上界為 $1$

### 5. 《綜合上述》
>證明 PLA 演算法的收斂性
> * 證明權重向量在經過 $t$ 次迭代之後是否會停止更新，即 $t$ 是否存在上界 ?
> * 假設數據 $D$ 是線性可分的， PLA 是否會停止，並找到這條線 ?

----
## 二、證明過程
### 《性質一》  
>$D$ 線性可分 $\Rightarrow$ $\exists \; w_f$ 使得 $y_n = sign(w_f^Tx_n)$，故 $y_n$ 與 $w_f^Tx_n$同號，即 $y_nw_f^Tx_n>0$

>對每一輪迭代和所有樣本點，均滿足 $y_{n(t)}w_f^Tx_{n(t)}\geq\underset{n}{\operatorname{min}}y_nw_f^Tx_n>0$

>### Recursive solve
$$
\begin{aligned}
w_f^T\cdot w_t &= w_f^T\cdot(w_{t-1}+y_{n(t-1)}x_{n(t-1)})\\
&=w_f^Tw_{t-1}+y_{n(t-1)}w_f^Tx_{n(t-1)}\\

&\geq w_f^Tw_{t-1}+\underset{n}{\operatorname{min}}y_nw_f^Tx_n\\

&\geq w_f^Tw_{t-2}+2\cdot\underset{n}{\operatorname{min}}y_nw_f^Tx_n\\

&\geq\cdots \\

&\geq w_f^Tw_0+t\cdot\underset{n}{\operatorname{min}}y_nw_f^Tx_n\\

&= t\cdot\underset{n}{\operatorname{min}}y_nw_f^Tx_n(\because w_0=0) 

\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots (1)


\end{aligned}
$$

### 《性質二》
>$w_t$只在錯誤分類下才更新，即$sign(w_t^Tx_{n(t)}) \not= y_{n(t)} \iff y_{n(t)}w_t^Tx_{n(t)}\leq0$ 

>### Recursive solve
$$
\begin{aligned}
\lVert w_t \rVert^2 

&= \lVert {w_{t-1}+y_{n(t-1)}}x_{n(t-1)} \rVert^2 \\

&=\lVert w_{t-1} \rVert^2+2\cdot y_{n(t-1)}w_{t-1}x_{n(t-1)}+\lVert y_{n(t-1)}x_{n(t-1)}\rVert^2\\

&\leq \lVert w_{t-1} \rVert^2+0+ \lVert y_{n(t-1)}x_{n(t-1)}\rVert^2\\

&\leq \lVert w_{t-1} \rVert^2+\underset{n}{\operatorname{max}}\lVert x_n \rVert^2\\

&\leq \lVert w_{t-2} \rVert^2+2\cdot \underset{n}{\operatorname{max}}\lVert x_n \rVert^2 \\

&\leq \lVert w_{0} \rVert^2+t\cdot \underset{n}{\operatorname{max}}\lVert x_n \rVert^2 

= t\cdot \underset{n}{\operatorname{max}}\lVert x_n \rVert^2(\because w_0=0)
\cdots\cdots\cdots\cdots\cdots\cdots\cdots\cdots (2)

\end{aligned}
$$

### 《結合$\;(1)\;(2)式子$》
$$
\begin{aligned}
    \frac{w_f^T}{\lVert w_f^T\rVert}\cdot\frac{w_t^T}{\lVert w_t^T\rVert}\leq\frac{t\cdot\underset{n}{\operatorname{min}}y_nw_f^Tx_n}{\lVert w_f^T\rVert \cdot \sqrt{t\cdot \underset{n}{\operatorname{max}}\lVert x_n \rVert^2 }}

    =\sqrt{t}\cdot\frac{\underset{n}{\operatorname{min}}y_n\frac{w_f^T}{\lVert w_f^T\rVert}x_n}{\sqrt{\underset{n}{\operatorname{max}}\lVert x_n \rVert^2}}=\sqrt{t} \cdot C \\
\end{aligned}  
$$  
可得 $C$ 為常數， $\frac{w_f^T}{\lVert w_f^T\rVert}\cdot\frac{w_t^T}{\lVert w_t^T\rVert}$ 隨迭代次數 $t$ 逐漸增長且 $\frac{w_f^T}{\lVert w_f^T\rVert}\cdot\frac{w_t^T}{\lVert w_t^T\rVert}\leq1$，因此

$$
\begin{aligned}
\sqrt{t}\cdot C\leq 1
\implies t \leq \frac{1}{C^2}\implies t \leq 

\frac{{\underset{n}{\operatorname{max}}\lVert x_n \rVert^2}}
{({\underset{n}{\operatorname{min}}y_n\frac{w_f^T}{\lVert w_f^T\rVert}x_n})^2}
=\frac{{\underset{n}{\operatorname{max}}\lVert x_n \rVert^2}}
{({\underset{n}{\operatorname{min}}\frac{w_f^T}{\lVert w_f^T\rVert}x_n})^2}
(\because y=\lbrace-1, +1\rbrace, y^2=1)，存在上界
\end{aligned}  
$$  
所以在線性可分情況下，PLA演算法最終會停止，找到$g(w_t) \approx f(w_f)$，得證。