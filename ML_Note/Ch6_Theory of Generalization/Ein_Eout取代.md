機器學習筆記-VC Dimension, Part II
筆記整理自台大林軒田老師的開放課程-機器學習基石，筆記中所有圖片來自於課堂講義。

  上一篇用成長函數$m_{\mathcal{H}}(N)$來衡量Hypotheses Set $\mathcal{H}$中有效的方程的數量(Effective Number of Hypotheses)，以取代Hoeffding’s Inequality中的大$M$，並用一種間接的方式 —- break point，來尋找$m_{\mathcal{H}}(N)$的上界，從而避免了直接研究$\mathcal{H}$的成長函數的困難。


學習所需”維他命”(The VC Dimension)
$$m_{\mathcal{H}}(N)\leq \sum_{i=0}^{k-1}\binom {N}{i}$$
  根據之前得到的式子，我們知道如果一個$\mathcal{H}$存在break point，我們就有辦法保證學出來的東西能夠“舉一反三”(good generalization)。一般來說break point越大的$\mathcal{H}$，其複雜度也更高，我們可以使用vc dimension來描述一個$\mathcal{H}$的複雜程度，這個vc dimension來自Vladimir Vapnik與Alexey Chervonenkis所提出的VC Theory。

  根據定義，一個$\mathcal{H}$的vc dimension(記為$d_{vc}(\mathcal{H})$)，是這個$\mathcal{H}$最多能夠shatter掉的點的數量 (the largest value of N for which $m_{\mathcal{H}}(N)=2^N$)，如果不管多少個點$\mathcal{H}$都能夠shatter他們，則$d_{vc}(H)=\infty$。不難看出$d_{vc}$與break point k的關係，有$k=d_{vc}+1$，因此我們用這個$d_{vc}$來描述成長函數的上界：
$$m_{\mathcal{H}}(N)\leq \sum_{i=0}^{d_{vc}} \binom {N}{i}$$
  上式右邊(RHS)事實上是最高項為$d_{vc}$的多項式，利用數學歸納法可得：
$$m_{\mathcal{H}}(N)\leq \sum_{i=0}^{d_{vc}} \binom {N}{i} \leq N^{d_{vc}}+1$$

更加一般化的Bound (The VC Generalization Bound)
  上一篇的末尾我們設想利用有限的$m_{\mathcal{H}}(N)$來替換無限的大$M$，得到$\mathcal{H}$遇到Bad Sample的概率上界：
$$\mathbb{P}_\mathcal{D}[BAD\ D]\leq 2m_{\mathcal{H}}(N)\cdot exp(-2\epsilon ^2N)$$
  其中$\mathbb{P}_\mathcal{D}[BAD\ D]$是$\mathcal{H}$中所有有效的方程(Effective Hypotheses)遇到Bad Sample的聯合概率，即$\mathcal{H}$中存在一個方程遇上bad sample，則說$\mathcal{H}$遇上bad sample。用更加精准的數學符號來表示上面的不等式：
$$\mathbb{P}[\exists h \in \mathcal{H}\text{ s.t. } |E_{in}(h)-E_{out}(h)|\gt \epsilon]\leq 2m_{\mathcal{H}}(N)\cdot exp(-2\epsilon ^2N)$$
  注：$\exists h \in \mathcal{H}\text{ s.t. }$ - $\mathcal{H}$中存在($\exists$)滿足($\text{ s.t }$)…的$h$

  但事實上上面的不等式是不嚴謹的，為什麼呢？$m_{\mathcal{H}}(N)$描述的是$\mathcal{H}$作用於資料量為$N$的資料$\mathcal{D}$，有效的方程數，因此$\mathcal{H}$當中每一個$h$作用於$\mathcal{D}$都能算出一個$E_{in}$來，一共能有$m_{\mathcal{H}}(N)$個不同的$E_{in}$，是一個有限的數。但在out of sample的世界裡(總體)，往往存在無限多個點，平面中任意一條直線，隨便轉一轉動一動，就能產生一個不同的$E_out$來。$E_{in}$的可能取值是有限個的，而$E_{out}$的可能取值是無限的，無法直接套用union bound，我們得先把上面那個無限多種可能的$E_{out}$換掉。那麼如何把$E_{out}$變成有限個呢？
  假設我們能從總體當中再獲得一份$N$筆的驗證資料(verification set)$\mathcal{D}’$，對於任何一個$h$我們可以算出它作用於$\mathcal{D}’$上的$E_{in}^{‘}$，由於$\mathcal{D}’$也是總體的一個樣本，因此如果$E_{in}$和$E_{out}$離很遠，有非常大的可能$E_{in}$和$E_{in}^{‘}$也會離得比較遠。



  事實上當N很大的時候，$E_{in}$和$E_{in}^{‘}$可以看做服從以$E_{out}$為中心的近似正態分佈(Gaussian)，如上圖。$[|E_{in}-E_{out}|\text{ is large}]$這個事件取決於$\mathcal{D}$，如果$[|E_{in}-E_{out}|\text{ is large}]$，則如果我們從總體中再抽一份$\mathcal{D}^{‘}$出來，有50%左右的可能性會發生$[|E_{in}-E_{in}^{‘}|\text{ is large}]$，還有大約50%的可能$[|E_{in}-E_{in}^{‘}|\text{ is not large}]$。
  因此，我們可以得到$\mathbb{P}[|E_{in}-E_{out}|\text{ is large}]$的一個大概的上界可以是$2\mathbb{P}[|E_{in}-E_{in}^{‘}|\text{ is large}]$，以此為啟發去尋找二者之間的關係。

  引理：

$$(1-2e^{-\frac{1}{2}\epsilon^2N})\mathbb{P}[\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{out}(h)| \gt \epsilon]\leq \mathbb{P}[\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{in}^{‘}(h)| \gt \frac{\epsilon}{2}]$$

  上面的不等式是從何而來的呢？我們先從RHS出發：

  上式第二行的不等號可以由$\mathbb{P}[\mathcal{B}_1]\geq \mathbb{P}[\mathcal{B}_1 \textbf{ and } \mathcal{B}_2]$得到，第三、四行則是貝葉斯公式，聯合概率等於先驗概率與條件概率之積。

  下面來看看不等式的最後一項$\mathbb{P}[\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{in}^{‘}(h)| \gt \frac{\epsilon}{2}\;\;|\;\;\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{out}(h)| \gt \epsilon]$。對於一個固定的data set $\mathcal{D}$來說，我們任選一個$h^{*}$使得$|E_{in}(h^{*})-E_{out}(h^{*})|\gt \epsilon$，注意到這個$h^{*}$只依賴於$\mathcal{D}$而不依賴於$\mathcal{D}^{‘}$噢，對於$\mathcal{D}^{‘}$來說可以認為這個$h^{*}$ is forced to pick out。

  由於$h^{*}$是對於$\mathcal{D}$來說滿足$|E_{in}-E_{out}|\gt \epsilon$的任意一個hypothesis，因此可以把式子中的上確界(sup)先去掉。

  這裡就要稍微出動一下前人的智慧了：

  為了直觀一點$h^{*}$就不寫了。經過各種去掉絕對值符號又加上絕對值符號的運算，可以發現LHS的兩個不等式是RHS那個不等式的充分非必要條件。而LHS第二個不等式是已知的，對於$h^{*}$必成立的。因此我們拿LHS這個充分非必要條件去替換RHS這個不等式，繼續前面的不等式：

  最後一個不等號動用了Hoeffding Inequality：

  之前說過對於$\mathcal{D}^{‘}$來說，$h^{*}$ is forced to pick out，因此$M=1$。接著把$\epsilon$替換為$\frac{\epsilon}{2}$，就成了$\mathbb{P}[|…|\lt \frac{\epsilon}{2}]\geq 2exp(-\frac{1}{2}\epsilon^2N)$。則我們可以得到引理中的不等式。

  對於$e^{-\frac{1}{2}e^2N}$，一個比較合理的要求是$e^{-\frac{1}{2}\epsilon^2N}\lt \frac{1}{4}$，譬如我們有400筆資料，想要$E_{in}$和$E_{out}$相差不超過0.1。注意到這只是一個bound，只要要求不太過分，也不能太寬鬆即可，適當的寬鬆一點是OK的。當然這裡也是想跟之前所說的 “$\mathbb{P}[|E_{in}-E_{out}|\text{ is large}]$的一個大概的上界可以是$2\mathbb{P}[|E_{in}-E_{in}^{‘}|\text{ is large}]$” 當中的2倍有所結合。

  所以就有$1-2e^{-\frac{1}{2}e^2N}\gt \frac{1}{2}$。帶回引理，可得：

$$\mathbb{P}[\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{out}(h)| \gt \epsilon]\leq 2\,\mathbb{P}[\underset{h\in \mathcal{H}}{sup}\ |E_{in}(h)-E_{in}^{‘}(h)| \gt \frac{\epsilon}{2}]$$

  這樣一來我們就把無限多種的$E_{out}$換成了有限多種的$E_{in}$，因為$\mathcal{D}$與$\mathcal{D}^{‘}$的大小相等，都為$N$，因此我們手中一共有$2N$筆資料，這樣$\mathcal{H}$作用於$\mathcal{D}+\mathcal{D}^{‘}$最多能產生$m_{\mathcal{H}}(2N)$種dichotomies。此時我們針對上面的不等式，就又可以使用union bound了。(關於union bound，可以參考上一篇VC Dimension, Part I)

  前面的動作相當於先從總體中抽出$2N$筆資料，把這$2N$筆資料當成一個比較小的bin，然後在這個bin中抽取$N$筆作為$\mathcal{D}$，剩下的$N$筆作為$\mathcal{D}^{‘}$，$\mathcal{D}$和$\mathcal{D}^{‘}$之間是沒有交集的。在我們想像出來的這個small bin當中，整個bin的錯誤率為$\frac{E_{in}+E_{out}}{2}$，又因為：

$$|E_{in}-E_{in}^{‘}|\gt \frac{\epsilon}{2} \Leftrightarrow |E_{in} - \frac{E_{in}+E_{in}^{‘}}{2}|\gt \frac{\epsilon}{4}$$

  所以用RHS替換LHS之後，前面不等式就又可以使用Hoeffding inequality了：
