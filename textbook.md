# Textbook For ML
----
## Machine Learning
    1. Learning From Data + ebooks(ch6 ~ ch9) + Solutions(ch1 ~ ch7)  
    
    2. Bishop - Pattern Recognition And Machine Learning + Solutions
   
    3. Murphy - Machine Learning A Probabilistic Perspective + Solutions
   
    4. The Elements of Statistical Learning + Solutions
    
    5. An Introduction to Statistical Learning with Applications in R
    
    6. Bayesian Reasoning and Machine Learning 

| Title | Paper Book | E-books|Solution|
| :---------|:---------:|:---------:|:---------:|
| Learning From Data | ○ |×| ch1~ ch7 |
| Pattern Recognition And Machine Learning | × |○| ○|
| Machine Learning A Probabilistic Perspective | × |○| ○|
| The Elements of Statistical Learning | × |○| ○|
|An Introduction to Statistical Learning with Applications in R| × |○|  ×|
| Bayesian Reasoning and Machine Learning | × |○|  ×|





## Optimization
    1. Practical Optimization: a Gentle Introduction
    
    2. Practical optimizations - Algorithms and Engineering Applications
    
    3. An Introduction to optimization
    
    4. Linear and Nonlinear Programming
    
    5. Applied Regression Analysis A Research Tool

## Image Processing and Computer Vision
    1. Digital Image Processing 4e + 3e Solutions
    
    2. Computer and Machine Vision 4e - Theory, Algorithms, Practicalities
    
    3. Computer Vision: Algorithms and Applications - Richard Szeliski
    
    4. Multiple View Geometry in Computer Vision 2e 
   

## Searching
    1. Learning with Kernels: Support Vector Machines, Regularization, Optimization, and Beyond
    
    2. Computer Vision: Models, Learning, and Inference
    
    3. A Gentle Introduction to Optimization 

## New Get
    1. Machine Learning: A Bayesian and Optimization Perspective
    
    2. Introdution to Linear Programming  
    
    3. Bishop - Pattern Recognition And Machine Learning + Solutions 所有資源

## Learning Resource

1. [協方差](https://blog.csdn.net/red_stone1/article/details/82754517)
   
2. [中國大神 Red Stone 台大ML筆記](https://blog.csdn.net/red_stone1/article/details/71082934)
   
3. [PRML Python程式碼](https://github.com/ctgk/PRML)

4. [Red Stone SVM](https://blog.csdn.net/red_stone1/article/details/84768163)

5. [Official Learning from data website(Gmail account and password)](http://book.caltech.edu/bookforum/showthread.php?t=4542)

6. [機器學習基石課後習題全(CSDN)](https://blog.csdn.net/a1015553840/article/details/51085129#reply)